!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'tests' stamp: 'HAW 5/22/2022 18:14:22'!
customerWithIdentificationType: anIdType number: anIdNumber 

	^ (session 
		select: [ :aCustomer | aCustomer identificationType = anIdType and: [ aCustomer identificationNumber = anIdNumber ]]
		ofType: Customer) anyOne! !

!ImportTest methodsFor: 'tests' stamp: 'mk 6/26/2022 21:16:39'!
setUp
	
	
	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction.
	! !

!ImportTest methodsFor: 'tests' stamp: 'HAW 5/22/2022 00:28:23'!
tearDown

	session commit.
	session close.
	! !

!ImportTest methodsFor: 'tests' stamp: 'mk 6/25/2022 17:00:53'!
test01Import
	
	CustomerImporter valueFrom: self validImportData into: session.
	
	self assertImportedRightNumberOfCustomers.
	self assertPepeSanchezWasImportedCorrecty.
	self assertJuanPerezWasImportedCorrectly ! !

!ImportTest methodsFor: 'tests' stamp: 'mk 6/25/2022 16:53:02'!
test02ImportInvalidKeyCustomer
	
	
	
	self should: [CustomerImporter valueFrom: (self InvalidCustomerData) into: session.]
	raise: Error 
	withExceptionDo:[:anError| self assert: CustomerImporter invalidKeyError equals: anError messageText.
						self assert: (session selectAllOfType: Customer) size equals: 1.
						] 
	! !

!ImportTest methodsFor: 'tests' stamp: 'mk 6/25/2022 16:55:54'!
test03ImportInvalidKeyAddress
	
	
	
	self should: [CustomerImporter valueFrom: (self InvalidAddressData ) into: session.]
	raise: Error 
	withExceptionDo:[:anError| self assert: CustomerImporter invalidKeyError  equals: anError messageText.
						self assert: (session selectAllOfType: Address) size equals: 0.
						] 
	! !

!ImportTest methodsFor: 'tests' stamp: 'mk 6/25/2022 18:08:40'!
test04raiseErrorWithWrongNumberOfFieldsForCustomer
	
	
	
	self should: [CustomerImporter valueFrom: (self invalidDataWithWrongNumberOfCustomerFields ) into: session.]
	raise: Error 
	withExceptionDo:[:anError| self assert: CustomerImporter wrongNumberOfFieldsError  equals: anError messageText.
						self assert: (session selectAllOfType: Customer) size equals: 0.
						]
	! !

!ImportTest methodsFor: 'tests' stamp: 'mk 6/25/2022 18:08:54'!
test05raiseErrorWithWrongNumberOfFieldsForAddress
	
	
	
	self should: [CustomerImporter valueFrom: (self invalidDataWithWrongNumberOfAddressFields ) into: session.]
	raise: Error 
	withExceptionDo:[:anError| self assert: CustomerImporter wrongNumberOfFieldsError  equals: anError messageText.
						self assert: (session selectAllOfType: Address) size equals: 0.
						]
	! !

!ImportTest methodsFor: 'tests' stamp: 'mk 6/25/2022 18:09:07'!
test06raiseErrorWithRecordMissing
	
	
	
	self should: [CustomerImporter valueFrom: (self invalidDataWithWrongLine ) into: session.]
	raise: Error 
	withExceptionDo:[:anError| self assert: CustomerImporter wrongLineError  equals: anError messageText.
						self assert: (session selectAllOfType: Address) size equals: 0.
						self assert: (session selectAllOfType: Customer) size equals: 1.
						]
	! !

!ImportTest methodsFor: 'tests' stamp: 'mk 6/26/2022 22:12:38'!
test07raiseErrorCustomerNotSpecified
	
	
	
	self should: [CustomerImporter valueFrom: (self invalidDataCustomerNotSpecified ) into: session.]
	raise: Error 
	withExceptionDo:[:anError| self assert: CustomerImporter customerNotSpecifiedError  equals: anError messageText.
						self assert: (session selectAllOfType: Address) size equals: 0.
						self assert: (session selectAllOfType: Customer) size equals: 0.
						]
	! !


!ImportTest methodsFor: 'asserts' stamp: 'HAW 5/22/2022 18:22:05'!
assertAddressOf: importedCustomer at: aStreetName hasNumber: aNumber town: aTown zipCode: aZipCode province: aProvince      

	| importedAddress |
		
	importedAddress := importedCustomer addressAt: aStreetName ifNone: [ self fail ].
	self assert: aStreetName equals: importedAddress streetName.
	self assert: aNumber equals: importedAddress streetNumber.
	self assert: aTown equals: importedAddress town.
	self assert: aZipCode equals: importedAddress zipCode.
	self assert: aProvince equals: importedAddress province.

	! !

!ImportTest methodsFor: 'asserts' stamp: 'HAW 5/22/2022 18:12:18'!
assertImportedRightNumberOfCustomers

	^ self assert: 2 equals: (session selectAllOfType: Customer) size! !

!ImportTest methodsFor: 'asserts' stamp: 'HAW 5/22/2022 18:22:05'!
assertJuanPerezWasImportedCorrectly

	| importedCustomer |
		
	importedCustomer := self customerWithIdentificationType: 'C' number: '23-25666777-9'.
		
	self assert: 'Juan' equals: importedCustomer firstName.
	self assert: 'Perez' equals: importedCustomer lastName.
	self assert: 'C' equals: importedCustomer identificationType.
	self assert: '23-25666777-9' equals: importedCustomer identificationNumber.

	self assertAddressOf: importedCustomer at: 'Alem' hasNumber: 1122 town: 'CABA' zipCode: 1001 province: 'CABA' 
	! !

!ImportTest methodsFor: 'asserts' stamp: 'HAW 5/22/2022 18:25:01'!
assertPepeSanchezWasImportedCorrecty

	| importedCustomer |
	
	importedCustomer := self customerWithIdentificationType: 'D' number: '22333444'.
		
	self assert: 'Pepe' equals: importedCustomer firstName.
	self assert: 'Sanchez' equals: importedCustomer lastName.
	self assert: 'D' equals: importedCustomer identificationType.
	self assert: '22333444' equals: importedCustomer identificationNumber.

	self assertAddressOf: importedCustomer at: 'San Martin' hasNumber: 3322 town: 'Olivos' zipCode: 1636 province: 'BsAs'.
	self assertAddressOf: importedCustomer at: 'Maipu' hasNumber: 888 town: 'Florida' zipCode: 1122 province: 'Buenos Aires'.
	
	
	! !


!ImportTest methodsFor: 'inputs' stamp: 'mk 6/25/2022 16:44:44'!
InvalidAddressData

	^ ReadStream on: 
'C,Pepe,Sanchez,D,22333444
A,Alem,1122,CABA,1001,CABA
As,Alem,1122,CABA,1001,CABA'! !

!ImportTest methodsFor: 'inputs' stamp: 'mk 6/25/2022 16:55:44'!
InvalidCustomerData

	^ ReadStream on: 'C,Pepe,Sanchez,D,22333444
Ca,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !

!ImportTest methodsFor: 'inputs' stamp: 'mk 6/26/2022 22:03:13'!
invalidDataCustomerNotSpecified

	^ ReadStream on: 
'A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !

!ImportTest methodsFor: 'inputs' stamp: 'mk 6/25/2022 17:59:35'!
invalidDataWithWrongLine

	^ ReadStream on: 
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs

A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !

!ImportTest methodsFor: 'inputs' stamp: 'mk 6/25/2022 17:34:44'!
invalidDataWithWrongNumberOfAddressFields

	^ ReadStream on: 'C,Pepe,Sanchez,D,22333444
C,Juan,Perez,C,23-25666777-9
A,Alem,1122'! !

!ImportTest methodsFor: 'inputs' stamp: 'mk 6/25/2022 17:17:13'!
invalidDataWithWrongNumberOfCustomerFields

	^ ReadStream on: 'C,Pepe,
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !

!ImportTest methodsFor: 'inputs' stamp: 'mk 6/25/2022 17:59:12'!
validImportData

	^ ReadStream on: 
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince
 
	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 17:55:46'!
isAt: aStreetName

	^streetName = aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName
 
	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber
 
	streetNumber := aStreetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown
 
	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode
 
	zipCode := aZipCode! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress 

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 17:55:17'!
addressAt: aStreetName ifNone: aNoneBlock

	^addresses detect: [ :address | address isAt: aStreetName ] ifNone: aNoneBlock ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName
	
	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName
 
	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName
 
	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber
	
	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber
 
	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType
	
	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType 

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !


!classDefinition: #CustomerImporter category: 'CustomerImporter'!
Object subclass: #CustomerImporter
	instanceVariableNames: 'session readStream line record newAddress newCustomer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporter methodsFor: 'initialization' stamp: 'HAW 5/22/2022 18:06:47'!
initializeFrom: aReadStream into: aSession
	session := aSession.
	readStream := aReadStream.! !


!CustomerImporter methodsFor: 'assert' stamp: 'mk 6/26/2022 22:09:36'!
assertCustomerIsDefined

	^ newCustomer isNil ifTrue:[ self error: self class customerNotSpecifiedError ]! !

!CustomerImporter methodsFor: 'assert' stamp: 'mk 6/25/2022 17:40:12'!
assertRecordHasCorrectNumberOfFields: numberOfFields

	record size ~= numberOfFields ifTrue:[^self error: self class wrongNumberOfFieldsError]! !


!CustomerImporter methodsFor: 'evaluating' stamp: 'pm 6/23/2022 20:38:52'!
hasNextLine

	line := readStream nextLine.
	^line notNil! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'mk 6/26/2022 22:09:36'!
importAddressRecord
	
	self assertCustomerIsDefined.	
	self assertRecordHasCorrectNumberOfFields: 6.
	newAddress := Address new.
			newCustomer addAddress: newAddress.
			newAddress streetName: record second.
			newAddress streetNumber: record third asNumber . 
			newAddress town: record fourth.
			newAddress zipCode: record fifth asNumber .
			newAddress province: record sixth! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'mk 6/25/2022 17:32:31'!
importCustomerRecord
	
	self assertRecordHasCorrectNumberOfFields: 5.
	newCustomer := Customer new.
			newCustomer firstName: record second.
			newCustomer lastName: record third.
			newCustomer identificationType: record fourth.
			newCustomer identificationNumber: record fifth.
			session persist: newCustomer! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'mk 6/25/2022 16:37:21'!
importRecord

	(self isCustomerRecord ) ifTrue: [^self importCustomerRecord ].
	(self isAddressRecord ) ifTrue: [^self importAddressRecord ].
		
	self error: self class invalidKeyError.! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'mk 6/25/2022 16:35:32'!
isAddressRecord

	^record first = 'A'.! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'mc 6/23/2022 21:23:01'!
isCustomerRecord

	^record first = 'C'! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'mk 6/25/2022 18:01:03'!
saveRecord
	
	line isEmpty ifTrue:[self error: self class wrongLineError ].
	record := line findTokens: $,
	! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'mk 6/26/2022 20:24:52'!
value 

	[ self hasNextLine ] whileTrue: [  
			self saveRecord.
			self importRecord.
			].
				
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImporter class' category: 'CustomerImporter'!
CustomerImporter class
	instanceVariableNames: ''!

!CustomerImporter class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 18:06:47'!
from: aReadStream into: aSession
	^self new initializeFrom: aReadStream into: aSession! !


!CustomerImporter class methodsFor: 'importing' stamp: 'HAW 5/22/2022 18:11:27'!
valueFrom: aReadStream into: aSession

	^(self from: aReadStream into: aSession) value! !


!CustomerImporter class methodsFor: 'error' stamp: 'mk 6/26/2022 22:08:05'!
customerNotSpecifiedError
	^'Cannot add address to undefined Customer'! !

!CustomerImporter class methodsFor: 'error' stamp: 'mk 6/25/2022 16:29:59'!
invalidAddressError
	^' Specified address is not valid'! !

!CustomerImporter class methodsFor: 'error' stamp: 'pm 6/23/2022 20:48:44'!
invalidCustomerError
	^'Specified customer is not valid.'! !

!CustomerImporter class methodsFor: 'error' stamp: 'mk 6/25/2022 16:38:11'!
invalidKeyError
	^'Invalid key.'! !

!CustomerImporter class methodsFor: 'error' stamp: 'mk 6/25/2022 17:16:30'!
wrongNumberOfFieldsError
	^'Wrong number of fields in line'! !


!CustomerImporter class methodsFor: 'documentation' stamp: 'mk 6/25/2022 17:57:28'!
wrongLineError
	^'Invalid Line'! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
beginTransaction

	! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
commit
	
	(tables at: Customer) do: [ :aCustomer | self persistAddressesOf: aCustomer ]
	! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 5/22/2022 00:19:29'!
close
	
	! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
persistAddressesOf: anObjectWithAddresses

	anObjectWithAddresses addresses do: [ :anAddress | self persist: anAddress ]
	! !


!DataBaseSession methodsFor: 'initialization' stamp: 'mk 6/25/2022 16:51:17'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new. 
	tables at: Customer put: Set new.
	tables at: Address put: Set new.
	id := 0.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 5/22/2022 00:19:29'!
persist: anObject

	| table |
	
	self delay.
	table := tables at: anObject class ifAbsentPut: [ Set new ].
	
	self defineIdOf: anObject.
	table add: anObject.
	
	(anObject isKindOf: Customer) ifTrue: [ self persistAddressesOf: anObject ].! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 00:19:29'!
select: aCondition ofType: aType

	self delay.
	^(tables at: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'pm 6/23/2022 21:13:46'!
selectAllOfType: aType

	self delay.
	^(tables at: aType) copy! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !
