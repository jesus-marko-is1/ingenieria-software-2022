!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'as yet unclassified' stamp: 'ms 6/20/2022 17:43:30'!
Stream

	^ ReadStream on:  'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'.! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'pm 6/22/2022 21:24:49'!
assert: aPerson hasFirstName: firstName lastName: lastName identifiedByType: identificationType andNumber: identificationNumber 
	
	self assert: aPerson firstName  = firstName.
	self assert: aPerson lastName = lastName.
	self assert: aPerson identificationType = identificationType.
	self assert: aPerson identificationNumber = identificationNumber.
	
	
	
	
	! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'pm 6/22/2022 21:33:21'!
assert: anAddres hasStreetName: aStreetName town: aTown province: aProvince streetNumber: aStreetNumber zipCode: aZipCode

	self assert: anAddres streetName = aStreetName.
	self assert: anAddres town = aTown.
	self assert: anAddres province = aProvince.
	self assert: anAddres streetNumber = aStreetNumber.
	self assert: anAddres zipCode = aZipCode.	
! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'mk 6/18/2022 15:37:03'!
setUp
	
	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'mk 6/18/2022 15:15:34'!
tearDown

	session commit.
	session close! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'pm 6/22/2022 21:26:20'!
test01ImportAndSaveCustomer

	| customers person inputStream |
	
	inputStream := self Stream.
	
	Import stream: inputStream session: session.
	
	
	customers := Customer new findByName: 'Pepe' in: session.
	person:= customers anyOne.
	
	self assert: customers size equals: 1.
	
	self assert: person hasFirstName: 'Pepe' lastName: 'Sanchez' identifiedByType: 'D' andNumber: '22333444'.
	
	
	
	! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'pm 6/22/2022 21:28:48'!
test02ImportAndSaveMoreThanOneCustomer

	| customers person inputStream   otherPerson |
	
	inputStream := self Stream.
	
	Import stream: inputStream session: session.
	
	
	customers := Customer new findByName: 'Pepe' in: session.
	person:= customers anyOne.
	
	self assert: person hasFirstName: 'Pepe' lastName: 'Sanchez' identifiedByType: 'D' andNumber: '22333444'.
	
	
	customers := Customer new findByName: 'Juan' in: session.
	
	otherPerson := customers anyOne.
	
	self assert: otherPerson hasFirstName: 'Juan' lastName: 'Perez' identifiedByType: 'C' andNumber: '23-25666777-9'.
	! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'pm 6/22/2022 21:37:56'!
test03ImportAddresByCustomer

	| customers person inputStream   addres anAddres |
	
	inputStream := self Stream.
	
	Import stream: inputStream session: session.
	
	customers := Customer new findByName: 'Pepe' in: session.
	person:= customers anyOne.
			
	addres := Address new of: person.
	self assert: addres size equals: 2.
	 
	anAddres:= addres first.
	
	self assert: anAddres hasStreetName: 'San Martin' town: 'Olivos' province: 'BsAs' streetNumber: 3322 zipCode: 1636.
	
	anAddres:= addres second.
	
	self assert: anAddres hasStreetName: 'Maipu' town: 'Florida' province: 'Buenos Aires' streetNumber: 888 zipCode: 1122.
	
! !

!ImportTest methodsFor: 'as yet unclassified' stamp: 'pm 6/22/2022 21:39:39'!
test04ImportAddresByAllCustomers

	| customers person inputStream   addres anAddres |
	
	inputStream := self Stream.
	
	Import stream: inputStream session: session.
	
	
	customers := Customer new findByName: 'Pepe' in: session.
	person:= customers anyOne.
			
	addres := Address new of: person.
	self assert: addres size equals: 2.
	 
	anAddres:= addres first.
	
	self assert: anAddres hasStreetName: 'San Martin' town: 'Olivos' province: 'BsAs' streetNumber: 3322 zipCode: 1636.
	
	anAddres:= addres second.
	
	self assert: anAddres hasStreetName: 'Maipu' town: 'Florida' province: 'Buenos Aires' streetNumber: 888 zipCode: 1122.
	
	customers := Customer new findByName: 'Juan' in: session.
	person:= customers anyOne.
	
	addres := Address new of: person.
	anAddres:= addres first.
	
	self assert: anAddres hasStreetName: 'Alem' town: 'CABA' province: 'CABA' streetNumber: 1122 zipCode: 1001.
! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!Address methodsFor: 'of' stamp: 'mk 6/22/2022 20:35:21'!
of: aCustomer 
	^aCustomer addresses.! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !


!Customer methodsFor: 'name' stamp: 'mk 6/21/2022 23:48:36'!
findByName: aName in: aSession

	^aSession select: [:customer | (customer firstName) = aName  ] ofType: Customer.! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Customer class' category: 'CustomerImporter'!
Customer class
	instanceVariableNames: ''!

!Customer class methodsFor: 'importing' stamp: 'HAW 5/22/2022 00:25:18'!
importCustomers

	"
	self importCustomers
	"
	| inputStream session newCustomer line |

	inputStream := StandardFileStream new open: 'input.txt' forWrite: false.
	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction.

	line := inputStream nextLine.
	[ line notNil ] whileTrue: [
		(line beginsWith: 'C') ifTrue: [ | customerData |
			customerData := line findTokens: $,.
			newCustomer := Customer new.
			newCustomer firstName: customerData second.
			newCustomer lastName: customerData third.
			newCustomer identificationType: customerData fourth.
			newCustomer identificationNumber: customerData fourth.
			session persist: newCustomer ].

		(line beginsWith: 'A') ifTrue: [ | addressData newAddress |
			addressData := line findTokens: $,.
			newAddress := Address new.
			newCustomer addAddress: newAddress.
			newAddress streetName: addressData second.
			newAddress streetNumber: addressData third asNumber .
			newAddress town: addressData fourth.
			newAddress zipCode: addressData fifth asNumber .
			newAddress province: addressData fourth ].

		line := inputStream nextLine. ].

	session commit.
	session close.

	inputStream close.
	! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
beginTransaction

	! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 19:17:36'!
commit

	(tables at: Customer ifAbsent: [#()]) do: [ :aCustomer | self persistAddressesOf: aCustomer ]
	! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 5/22/2022 00:19:29'!
close

	! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 19:29:06'!
objectsOfType: aType

	^ tables at: aType ifAbsent: [ #() ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
persistAddressesOf: anObjectWithAddresses

	anObjectWithAddresses addresses do: [ :anAddress | self persist: anAddress ]
	! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 5/22/2022 00:19:29'!
persist: anObject

	| table |

	self delay.
	table := tables at: anObject class ifAbsentPut: [ Set new ].

	self defineIdOf: anObject.
	table add: anObject.

	(anObject isKindOf: Customer) ifTrue: [ self persistAddressesOf: anObject ].! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
select: aCondition ofType: aType

	self delay.
	^(self objectsOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
selectAllOfType: aType

	self delay.
	^(self objectsOfType: aType) copy ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !


!classDefinition: #Import category: 'CustomerImporter'!
Object subclass: #Import
	instanceVariableNames: 'stream session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Import methodsFor: 'initialization' stamp: 'ms 6/20/2022 17:00:53'!
initializeStream: aStream session: aSession
	stream := aStream.
	session := aSession.
	
	^self customerImporter.! !


!Import methodsFor: 'evaluating' stamp: 'mk 6/21/2022 22:09:39'!
customerImporter
	
	| newCustomer line |

	line := stream nextLine.
	[ line notNil ] whileTrue: [
		(line beginsWith: 'C') ifTrue: [ | customerData |
			customerData := line findTokens: $,.
			newCustomer := Customer new.
			newCustomer firstName: customerData second.
			newCustomer lastName: customerData third.
			newCustomer identificationType: customerData fourth.
			newCustomer identificationNumber: customerData fifth .
			session persist: newCustomer ].

		(line beginsWith: 'A') ifTrue: [ | addressData newAddress |
			addressData := line findTokens: $,.
			newAddress := Address new.
			newCustomer addAddress: newAddress.
			newAddress streetName: addressData second.
			newAddress streetNumber: addressData third asNumber .
			newAddress town: addressData fourth.
			newAddress zipCode: addressData fifth asNumber .
			newAddress province: addressData sixth.
			 ].

		line := stream nextLine. ].



	stream close.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Import class' category: 'CustomerImporter'!
Import class
	instanceVariableNames: ''!

!Import class methodsFor: 'instance creation' stamp: 'ms 6/20/2022 16:50:55'!
stream: aStream session: aSession
	^self new initializeStream: aStream session: aSession! !
