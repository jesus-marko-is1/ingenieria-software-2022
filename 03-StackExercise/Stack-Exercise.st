!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 9/16/2021 17:40:17'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:24'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:31'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 21:08:23'!
test01ShouldNotFindAnythingInEmptyStack
	
	| sentenceFinder stack |
	
	sentenceFinder := SentenceFinderByPrefix new.
	
	stack := OOStack new.
	
	self assert: (sentenceFinder find: 'prefix' in: stack) isEmpty.
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 21:34:07'!
test02ShouldIncludeAllElementsGivenNoPrefix
	
	| sentenceFinder stack firstElement secondElement expectedElements returnedElements |
	
	sentenceFinder := SentenceFinderByPrefix new.
	
	firstElement := 'Hola'.
	secondElement := 'Que tal?'.
			
	stack := OOStack new.
	
	stack push: firstElement.
	stack push: secondElement.
	
	expectedElements := OrderedCollection new.
	expectedElements add: secondElement. 
	expectedElements add: firstElement.
	
	returnedElements := sentenceFinder find: '' in: stack.
	
	self assert: ( returnedElements =  expectedElements).
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 21:31:32'!
test03ShouldIncludeTheOnlyElementWhenItHasPrefix
	
	| sentenceFinder stack expectedElements returnedElements onlyElement |
	
	sentenceFinder := SentenceFinderByPrefix new.
	
	onlyElement := 'Que tal?'.
		
	stack := OOStack new.
	
	stack push: onlyElement.
	
	expectedElements := OrderedCollection new.
	expectedElements add: onlyElement.
	
	returnedElements := sentenceFinder find: 'Que' in: stack.
	
	self assert: ( returnedElements =  expectedElements ).
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'pm 4/27/2022 18:23:29'!
test04ShouldIncludeAllElementsWhenTheyMatchPrefix
	
	| sentenceFinder stack firstElement secondElement expectedElements returnedElements |
	
	sentenceFinder := SentenceFinderByPrefix new.
	
	firstElement := 'Hola, que tal?'.
	secondElement := 'Hola, todo bien'.
			
	stack := OOStack new.
	
	stack push: firstElement.
	stack push: secondElement.
	
	expectedElements := OrderedCollection new.
	expectedElements add: secondElement. 
	expectedElements add: firstElement.
	
	returnedElements := sentenceFinder find: 'Hola' in: stack.
	
	self assert: ( returnedElements =  expectedElements).
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'pm 4/27/2022 18:24:59'!
test05ShouldNotFindAnythingWhenThereIsNoPrefixMatch
	
	| sentenceFinder stack firstElement secondElement expectedElements returnedElements |
	
	sentenceFinder := SentenceFinderByPrefix new.
	
	firstElement := 'Hola, que tal?'.
	secondElement := 'Hola, todo bien'.
			
	stack := OOStack new.
	
	stack push: firstElement.
	stack push: secondElement.
	
	expectedElements := OrderedCollection new.
	
	returnedElements := sentenceFinder find: 'Chau' in: stack.
	
	self assert: ( returnedElements =  expectedElements).
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'pm 4/27/2022 18:42:07'!
test06ShouldIncludeOnlyTheElementsWhichMatchPrefix
	
	| sentenceFinder stack firstElement secondElement expectedElements returnedElements thirdElement |
	
	sentenceFinder := SentenceFinderByPrefix new.
	
	firstElement := 'Hola, que tal?'.
	secondElement := 'Hola, todo bien'.
	thirdElement := 'Chau, gracias'.
			
	stack := OOStack new.
	
	stack push: firstElement.
	stack push: secondElement.
	stack push: thirdElement.
		
	expectedElements := OrderedCollection new.
	expectedElements add: secondElement. 
	expectedElements add: firstElement.
	
	returnedElements := sentenceFinder find: 'Hola' in: stack.
	
	self assert: ( returnedElements =  expectedElements).
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'peto 4/27/2022 21:54:51'!
test07ShouldRespectCaseWhenMatchingPrefix
	
	| sentenceFinder stack firstElement secondElement expectedElements returnedElements |
	
	sentenceFinder := SentenceFinderByPrefix new.
	
	firstElement := 'Hola, que tal?'.
	secondElement := 'HOla, todo bien'.
			
	stack := OOStack new.
	
	stack push: firstElement.
	stack push: secondElement.
		
	expectedElements := OrderedCollection new. 
	expectedElements add: firstElement.
	
	returnedElements := sentenceFinder find: 'Hola' in: stack.
	
	self assert: ( returnedElements =  expectedElements).
	
	! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'tope'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 19:23:07'!
initialize

	tope := Base new.! !

!OOStack methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 19:29:12'!
isEmpty

	^tope indicatesEmpty.! !

!OOStack methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 19:53:53'!
pop
	| oldTopContent |
	
	oldTopContent := self top.
	
	tope := tope bellow.
	
	^oldTopContent
	! !

!OOStack methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 19:37:27'!
push: aString
	
	| newElement |
	
	newElement := Element new initializeWith: aString stackOnto: tope.
	
	tope := newElement! !

!OOStack methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 19:47:37'!
size
	^tope slotsToBase. ! !

!OOStack methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 19:45:32'!
top
	^tope content.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 9/16/2021 17:39:43'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'as yet unclassified' stamp: 'pm 4/27/2022 19:48:24'!
find: aStringPrefix in: aStack
	
	| sentencesWithPrefix |
	
	sentencesWithPrefix := OrderedCollection new.
	
	self untilEmpty: aStack do: [  | topContent |
		topContent := aStack pop.
		(topContent beginsWith: aStringPrefix) ifTrue: [sentencesWithPrefix add: topContent]	
	].

	^sentencesWithPrefix.
	! !


!SentenceFinderByPrefix methodsFor: 'private' stamp: 'pm 4/27/2022 18:51:31'!
untilEmpty: aCollection do: aClosure

	[aCollection isEmpty] whileFalse: aClosure.
	! !


!classDefinition: #Slot category: 'Stack-Exercise'!
Object subclass: #Slot
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!Slot methodsFor: 'as yet unclassified' stamp: 'pm 4/27/2022 19:32:10'!
content
	
	self subclassResponsibility.! !

!Slot methodsFor: 'as yet unclassified' stamp: 'pm 4/27/2022 19:32:03'!
indicatesEmpty
	
	self subclassResponsibility.! !

!Slot methodsFor: 'as yet unclassified' stamp: 'pm 4/27/2022 19:32:21'!
slotsToBase
	
	self subclassResponsibility.! !


!classDefinition: #Base category: 'Stack-Exercise'!
Slot subclass: #Base
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!Base methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 19:56:09'!
content
	
	^self error: OOStack stackEmptyErrorDescription.! !

!Base methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 19:28:30'!
indicatesEmpty
	^true.! !

!Base methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 19:47:57'!
slotsToBase
	^0! !


!classDefinition: #Element category: 'Stack-Exercise'!
Slot subclass: #Element
	instanceVariableNames: 'content bellow'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!Element methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 19:38:58'!
bellow
	^bellow.! !

!Element methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 19:39:18'!
content
	^content.! !

!Element methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 19:28:38'!
indicatesEmpty
	^false.! !

!Element methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 19:36:26'!
initializeWith: aString stackOnto: aSlot
	
	content := aString.
	bellow := aSlot! !

!Element methodsFor: 'as yet unclassified' stamp: 'pm 4/26/2022 19:48:41'!
slotsToBase
	^bellow slotsToBase + 1! !
