!classDefinition: #MarsRoverLoggerTest category: 'MarsRover-WithHeading'!
TestCase subclass: #MarsRoverLoggerTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverLoggerTest methodsFor: 'as yet unclassified' stamp: 'mk 6/1/2022 21:01:32'!
test01DoesNotLogWhenNoCommand

	| marsRover logger |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	logger := MarsRoverLogger followPositionChangesOn: marsRover.
	
	marsRover process: ''.
	
	self assert: logger readStream atEnd.
! !

!MarsRoverLoggerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/1/2022 22:57:04'!
test02LogsPositionWhenCommandForward

	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	logger := MarsRoverLogger followPositionChangesOn: marsRover.
	
	marsRover process: 'f'.
	
	logStream := logger readStream.
	self assert: '1@2' equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverLoggerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/1/2022 22:57:15'!
test03LogsPositionWhenCommandBackward

	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	logger := MarsRoverLogger followPositionChangesOn: marsRover.
	
	marsRover process: 'b'.
	
	logStream := logger readStream.
	self assert: '1@0' equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverLoggerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/1/2022 22:57:38'!
test04LogsPositionOnManyPositionCommands

	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	logger := MarsRoverLogger followPositionChangesOn: marsRover.
	
	marsRover process: 'fb'.
	
	logStream := logger readStream.
	self assert: '2@1' equals: logStream nextLine.
	self assert: '1@1' equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverLoggerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/1/2022 22:57:59'!
test05LogsHeadingWhenCommandRight

	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	logger := MarsRoverLogger followHeadingChangesOn: marsRover.
	
	marsRover process: 'r'.
	
	logStream := logger readStream.
	self assert: 'East' equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverLoggerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/1/2022 22:58:17'!
test06LogsHeadingsOnManyRotateCommands

	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	logger := MarsRoverLogger followHeadingChangesOn: marsRover.
	
	marsRover process: 'lll'.
	
	logStream := logger readStream.
	self assert: 'North' equals: logStream nextLine.
	self assert: 'West' equals: logStream nextLine.
	self assert: 'South' equals: logStream nextLine.
		
	self assert: logStream atEnd.
! !

!MarsRoverLoggerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/1/2022 22:58:48'!
test07LogsPositionAndHeadingOnCombinedCommands

	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	logger := MarsRoverLogger followPositionAndHeadingChangesOn: marsRover.
	
	marsRover process: 'fr'.
	
	logStream := logger readStream.
	self assert: '2@1' equals: logStream nextLine.
	self assert: 'South' equals: logStream nextLine.
		
	self assert: logStream atEnd.
! !


!classDefinition: #MarsRoverTest category: 'MarsRover-WithHeading'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:21:23'!
test01DoesNotMoveWhenNoCommand

	self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: '' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:12'!
test02IsAtFailsForDifferentPosition

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@2 heading: self north)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:31'!
test03IsAtFailsForDifferentHeading

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@1 heading: self south)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:17'!
test04IncrementsYAfterMovingForwardWhenHeadingNorth

	self 
		assertIsAt: 1@3 
		heading: self north 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:11'!
test06DecrementsYAfterMovingBackwardsWhenHeadingNorth

	self 
		assertIsAt: 1@1 
		heading: self north 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:59'!
test07PointToEashAfterRotatingRightWhenHeadingNorth

	self 
		assertIsAt: 1@2 
		heading: self east 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:51'!
test08PointsToWestAfterRotatingLeftWhenPointingNorth

	self 
		assertIsAt: 1@2 
		heading: self west 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:45'!
test09DoesNotProcessInvalidCommand

	| marsRover |
	
	marsRover := MarsRover at: 1@2 heading: self north.
	
	self 
		should: [ marsRover process: 'x' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: marsRover invalidCommandErrorDescription.
			self assert: (marsRover isAt: 1@2 heading: self north) ]! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:39'!
test10CanProcessMoreThanOneCommand

	self 
		assertIsAt: 1@4 
		heading: self north 
		afterProcessing: 'ff' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:31'!
test11IncrementsXAfterMovingForwareWhenHeadingEast

	self 
		assertIsAt: 2@2 
		heading: self east 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:19'!
test12DecrementsXAfterMovingBackwardWhenHeadingEast

	self 
		assertIsAt: 0@2 
		heading: self east 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:14'!
test13PointsToSouthAfterRotatingRightWhenHeadingEast

		self 
		assertIsAt: 1@2 
		heading: self south 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:05'!
test14PointsToNorthAfterRotatingLeftWhenPointingEast

		self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:00'!
test15ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingSouth

	self 
		assertIsAt: 1@1 
		heading: self west 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self south 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:52'!
test16ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingWest

	self 
		assertIsAt: 0@2 
		heading: self north 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self west 
! !


!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:31'!
east

	^ MarsRoverHeadingEast ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:38'!
north

	^ MarsRoverHeadingNorth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:45'!
south

	^ MarsRoverHeadingSouth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:54'!
west

	^ MarsRoverHeadingWest ! !


!MarsRoverTest methodsFor: 'assertions' stamp: 'HAW 10/7/2021 20:20:47'!
assertIsAt: newPosition heading: newHeadingType afterProcessing: commands whenStartingAt: startPosition heading: startHeadingType

	| marsRover |
	
	marsRover := MarsRover at: startPosition heading: startHeadingType. 
	
	marsRover process: commands.
	
	self assert: (marsRover isAt: newPosition heading: newHeadingType)! !


!classDefinition: #WindowTest category: 'MarsRover-WithHeading'!
TestCase subclass: #WindowTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!WindowTest methodsFor: 'as yet unclassified' stamp: 'mk 6/1/2022 20:58:58'!
test01DoesNotLogWhenNoCommand

	| marsRover window |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	window := MarsRoverWindow followPositionChangesOn: marsRover.
	
	marsRover process: ''.
	
	self assert: window position equals: nil.
	
	! !

!WindowTest methodsFor: 'as yet unclassified' stamp: 'pm 6/1/2022 22:59:40'!
test02LogsPositionOnForwardCommand

	| marsRover window |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	window := MarsRoverWindow followPositionChangesOn: marsRover.
	
	marsRover process: 'f'.
	
	self assert: window position equals: 1@2.
	
	! !

!WindowTest methodsFor: 'as yet unclassified' stamp: 'pm 6/1/2022 23:00:14'!
test03LogsHeadingsOnManyRotateCommands

	| marsRover window |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	window := MarsRoverWindow followHeadingChangesOn: marsRover.
	
	marsRover process: 'rrrrr'.
	
	self assert: window heading equals: 'East'.
	
	! !

!WindowTest methodsFor: 'as yet unclassified' stamp: 'pm 6/1/2022 23:00:40'!
test04LogsPositionAndHeadingOnCombinedCommands

	| marsRover window |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	window := MarsRoverWindow followPositionAndHeadingChangesOn: marsRover.
	
	marsRover process: 'ffll'.
	
	self assert: window heading equals: 'South'.
	self assert: window position equals: 1@3.	
	! !


!classDefinition: #Logger category: 'MarsRover-WithHeading'!
Object subclass: #Logger
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!Logger methodsFor: 'heading' stamp: 'mk 6/1/2022 20:42:58'!
headingChangedTo: aMarsRoverHeading
	self subclassResponsibility.! !

!Logger methodsFor: 'heading' stamp: 'mk 6/1/2022 20:45:06'!
visitMarsRoverHeadingEast: aMarsRoverHeadingEast
	self subclassResponsibility ! !

!Logger methodsFor: 'heading' stamp: 'mk 6/1/2022 20:44:39'!
visitMarsRoverHeadingNorth: aMarsRoverHeadingEast
	self subclassResponsibility ! !

!Logger methodsFor: 'heading' stamp: 'mk 6/1/2022 20:44:57'!
visitMarsRoverHeadingSouth: aMarsRoverHeadingEast
	self subclassResponsibility ! !

!Logger methodsFor: 'heading' stamp: 'mk 6/1/2022 20:44:49'!
visitMarsRoverHeadingWest: aMarsRoverHeadingEast
	self subclassResponsibility ! !


!Logger methodsFor: 'position' stamp: 'mk 6/1/2022 20:43:21'!
positionChangedTo: aMarsRoverPosition
	self subclassResponsibility.! !


!Logger methodsFor: 'followChanges' stamp: 'pm 6/1/2022 19:59:31'!
followHeadingChangesOn: aMarsRover 
	
	aMarsRover addObserver: (HeadingObserver for: aMarsRover with: self).! !

!Logger methodsFor: 'followChanges' stamp: 'pm 6/1/2022 19:59:45'!
followPositionAndHeadingChangesOn: aMarsRover 
	
	self followPositionChangesOn: aMarsRover.
	self followHeadingChangesOn: aMarsRover.! !

!Logger methodsFor: 'followChanges' stamp: 'pm 6/1/2022 20:00:00'!
followPositionChangesOn: aMarsRover 
	
	aMarsRover addObserver: (PositionObserver for: aMarsRover with: self).! !


!classDefinition: #MarsRoverLogger category: 'MarsRover-WithHeading'!
Logger subclass: #MarsRoverLogger
	instanceVariableNames: 'log'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverLogger methodsFor: 'initialization' stamp: 'HAW 5/30/2022 20:25:00'!
initialize

	log  := WriteStream on: ''! !


!MarsRoverLogger methodsFor: 'readStream' stamp: 'HAW 5/30/2022 20:23:26'!
readStream
	
	^ReadStream on: log contents ! !


!MarsRoverLogger methodsFor: 'heading' stamp: 'HAW 5/30/2022 20:46:00'!
headingChangedTo: aMarsRoverHeading
	
	aMarsRoverHeading accept: self! !

!MarsRoverLogger methodsFor: 'heading' stamp: 'HAW 5/30/2022 20:46:36'!
visitMarsRoverHeadingEast: aMarsRoverHeadingEast 
	
	log nextPutAll: 'East'; newLine! !

!MarsRoverLogger methodsFor: 'heading' stamp: 'pm 5/30/2022 21:44:41'!
visitMarsRoverHeadingNorth: aMarsRoverHeadingNorth
	
	log nextPutAll: 'North'; newLine! !

!MarsRoverLogger methodsFor: 'heading' stamp: 'pm 5/30/2022 21:44:50'!
visitMarsRoverHeadingSouth: aMarsRoverHeadingWest
	
	log nextPutAll: 'South'; newLine! !

!MarsRoverLogger methodsFor: 'heading' stamp: 'pm 5/30/2022 21:08:14'!
visitMarsRoverHeadingWest: aMarsRoverHeadingEast 
	
	log nextPutAll: 'West'; newLine! !


!MarsRoverLogger methodsFor: 'position' stamp: 'HAW 5/30/2022 20:22:16'!
positionChangedTo: aPosition

	log print: aPosition; newLine! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverLogger class' category: 'MarsRover-WithHeading'!
MarsRoverLogger class
	instanceVariableNames: ''!

!MarsRoverLogger class methodsFor: 'as yet unclassified' stamp: 'HAW 5/30/2022 20:42:45'!
followHeadingChangesOn: aMarsRover 
	
	^self new followHeadingChangesOn: aMarsRover ! !

!MarsRoverLogger class methodsFor: 'as yet unclassified' stamp: 'pm 5/30/2022 21:11:41'!
followPositionAndHeadingChangesOn: aMarsRover 
	^self new followPositionAndHeadingChangesOn: aMarsRover! !

!MarsRoverLogger class methodsFor: 'as yet unclassified' stamp: 'HAW 5/30/2022 20:16:11'!
followPositionChangesOn: aMarsRover 
	
	^self new followPositionChangesOn: aMarsRover ! !


!classDefinition: #MarsRoverWindow category: 'MarsRover-WithHeading'!
Logger subclass: #MarsRoverWindow
	instanceVariableNames: 'position heading'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverWindow methodsFor: 'position' stamp: 'pm 5/30/2022 21:32:23'!
position

	^position.! !

!MarsRoverWindow methodsFor: 'position' stamp: 'pm 5/30/2022 21:32:02'!
positionChangedTo: aPosition

	position := aPosition! !


!MarsRoverWindow methodsFor: 'heading' stamp: 'pm 5/30/2022 21:39:29'!
heading
	^heading! !

!MarsRoverWindow methodsFor: 'heading' stamp: 'pm 5/30/2022 21:42:12'!
headingChangedTo: aMarsRoverHeading
	
	aMarsRoverHeading accept: self! !

!MarsRoverWindow methodsFor: 'heading' stamp: 'pm 5/30/2022 21:43:36'!
visitMarsRoverHeadingEast: aMarsRoverHeadingEast

	heading := 'East'.! !

!MarsRoverWindow methodsFor: 'heading' stamp: 'pm 5/30/2022 21:44:02'!
visitMarsRoverHeadingNorth: aMarsRoverHeadingNorth

	heading := 'North'.! !

!MarsRoverWindow methodsFor: 'heading' stamp: 'pm 5/30/2022 21:43:48'!
visitMarsRoverHeadingSouth: aMarsRoverHeadingSouth

	heading := 'South'.! !

!MarsRoverWindow methodsFor: 'heading' stamp: 'pm 5/30/2022 21:43:21'!
visitMarsRoverHeadingWest: aMarsRoverHeadingWest

	heading := 'West'.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverWindow class' category: 'MarsRover-WithHeading'!
MarsRoverWindow class
	instanceVariableNames: ''!

!MarsRoverWindow class methodsFor: 'position' stamp: 'pm 5/30/2022 21:27:51'!
followPositionChangesOn: aMarsRover 
	
	^self new followPositionChangesOn: aMarsRover.! !


!MarsRoverWindow class methodsFor: 'instance creation' stamp: 'pm 5/30/2022 21:40:39'!
followHeadingChangesOn: aMarsRover 
	^self new followHeadingChangesOn: aMarsRover ! !

!MarsRoverWindow class methodsFor: 'instance creation' stamp: 'pm 5/30/2022 21:47:46'!
followPositionAndHeadingChangesOn: aMarsRover 

	^self new followPositionAndHeadingChangesOn: aMarsRover ! !


!classDefinition: #MarsRover category: 'MarsRover-WithHeading'!
Object subclass: #MarsRover
	instanceVariableNames: 'position head observers'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:48:45'!
invalidCommandErrorDescription
	
	^'Invalid command'! !

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:50:26'!
signalInvalidCommand
	
	self error: self invalidCommandErrorDescription ! !


!MarsRover methodsFor: 'initialization' stamp: 'pm 6/1/2022 19:50:59'!
initializeAt: aPosition heading: aHeadingType

	position := aPosition.
	head := aHeadingType for: self.
	observers := OrderedCollection new.! !


!MarsRover methodsFor: 'heading' stamp: 'pm 5/30/2022 21:17:47'!
headEast
	
	head := MarsRoverHeadingEast for: self.
	self logHeadingChange.! !

!MarsRover methodsFor: 'heading' stamp: 'pm 5/30/2022 21:17:42'!
headNorth
	
	head := MarsRoverHeadingNorth for: self.
	self logHeadingChange.

! !

!MarsRover methodsFor: 'heading' stamp: 'pm 5/30/2022 21:17:37'!
headSouth
	
	head := MarsRoverHeadingSouth for: self.
	self logHeadingChange.! !

!MarsRover methodsFor: 'heading' stamp: 'pm 5/30/2022 21:17:28'!
headWest
	
	head := MarsRoverHeadingWest for: self.
	self logHeadingChange.! !

!MarsRover methodsFor: 'heading' stamp: 'pm 6/1/2022 19:49:08'!
heading

	^head.! !

!MarsRover methodsFor: 'heading' stamp: 'pm 6/1/2022 19:44:59'!
logHeadingChange

	self notify.

! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	head rotateLeft! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	head rotateRight! !


!MarsRover methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:16:32'!
isAt: aPosition heading: aHeadingType

	^position = aPosition and: [ head isHeading: aHeadingType ]! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:51'!
isBackwardCommand: aCommand

	^aCommand = $b! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:19'!
isForwardCommand: aCommand

	^aCommand = $f ! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:51'!
isRotateLeftCommand: aCommand

	^aCommand = $l! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:21'!
isRotateRightCommand: aCommand

	^aCommand = $r! !


!MarsRover methodsFor: 'moving' stamp: 'pm 6/1/2022 19:44:44'!
addToPositionAndLogNewPosition: newPosition

	position := position + newPosition.
	self notify.! !

!MarsRover methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	head moveBackward! !

!MarsRover methodsFor: 'moving' stamp: 'HAW 5/30/2022 20:38:44'!
moveEast
	
	self addToPositionAndLogNewPosition: 1@0.
	! !

!MarsRover methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	head moveForward! !

!MarsRover methodsFor: 'moving' stamp: 'HAW 5/30/2022 20:38:44'!
moveNorth
	
	self addToPositionAndLogNewPosition: 0@1.
	! !

!MarsRover methodsFor: 'moving' stamp: 'HAW 5/30/2022 20:38:44'!
moveSouth
	
	self addToPositionAndLogNewPosition: 0@-1.
	! !

!MarsRover methodsFor: 'moving' stamp: 'HAW 5/30/2022 20:38:44'!
moveWest
	
	self addToPositionAndLogNewPosition: -1@0! !

!MarsRover methodsFor: 'moving' stamp: 'pm 6/1/2022 19:49:36'!
position 

	^position.! !


!MarsRover methodsFor: 'command processing' stamp: 'HAW 6/30/2018 19:48:26'!
process: aSequenceOfCommands

	aSequenceOfCommands do: [:aCommand | self processCommand: aCommand ]
! !

!MarsRover methodsFor: 'command processing' stamp: 'HAW 8/22/2019 12:08:50'!
processCommand: aCommand

	(self isForwardCommand: aCommand) ifTrue: [ ^ self moveForward ].
	(self isBackwardCommand: aCommand) ifTrue: [ ^ self moveBackward ].
	(self isRotateRightCommand: aCommand) ifTrue: [ ^ self rotateRight ].
	(self isRotateLeftCommand: aCommand) ifTrue: [ ^ self rotateLeft ].

	self signalInvalidCommand.! !


!MarsRover methodsFor: 'observer' stamp: 'pm 6/1/2022 19:53:26'!
addObserver: anObserver

	observers add: anObserver.! !

!MarsRover methodsFor: 'observer' stamp: 'pm 6/1/2022 20:06:20'!
notify

	observers do: [:anObserver | anObserver update].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover-WithHeading'!
MarsRover class
	instanceVariableNames: 'headings'!

!MarsRover class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:10:30'!
at: aPosition heading: aHeadingType
	
	^self new initializeAt: aPosition heading: aHeadingType! !


!classDefinition: #MarsRoverHeading category: 'MarsRover-WithHeading'!
Object subclass: #MarsRoverHeading
	instanceVariableNames: 'marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:15:38'!
isHeading: aHeadingType

	^self isKindOf: aHeadingType ! !


!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'accept' stamp: 'mk 6/1/2022 20:56:38'!
accept: aVisitor
	self subclassResponsibility. ! !


!MarsRoverHeading methodsFor: 'initialization' stamp: 'HAW 10/7/2021 20:11:59'!
initializeFor: aMarsRover 
	
	marsRover := aMarsRover.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeading class' category: 'MarsRover-WithHeading'!
MarsRoverHeading class
	instanceVariableNames: ''!

!MarsRoverHeading class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:11:35'!
for: aMarsRover 
	
	^self new initializeFor: aMarsRover ! !


!classDefinition: #MarsRoverHeadingEast category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingEast
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveWest! !

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveEast! !


!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headNorth! !

!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headSouth! !


!MarsRoverHeadingEast methodsFor: 'accept' stamp: 'HAW 5/30/2022 20:46:21'!
accept: aVisitor

	^aVisitor visitMarsRoverHeadingEast: self! !


!classDefinition: #MarsRoverHeadingNorth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingNorth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveSouth! !

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveNorth! !


!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headWest! !

!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headEast! !


!MarsRoverHeadingNorth methodsFor: 'accept' stamp: 'pm 5/30/2022 21:06:53'!
accept: aVisitor

	^aVisitor visitMarsRoverHeadingNorth: self! !


!classDefinition: #MarsRoverHeadingSouth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingSouth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveNorth! !

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveSouth! !


!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headEast! !

!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headWest! !


!MarsRoverHeadingSouth methodsFor: 'accept' stamp: 'pm 5/30/2022 21:07:15'!
accept: aVisitor

	^aVisitor visitMarsRoverHeadingSouth: self! !


!classDefinition: #MarsRoverHeadingWest category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingWest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	^marsRover moveEast! !

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveWest! !


!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headSouth! !

!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headNorth! !


!MarsRoverHeadingWest methodsFor: 'accept' stamp: 'pm 5/30/2022 21:07:29'!
accept: aVisitor

	^aVisitor visitMarsRoverHeadingWest: self! !


!classDefinition: #MarsRoverObserver category: 'MarsRover-WithHeading'!
Object subclass: #MarsRoverObserver
	instanceVariableNames: 'logger marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverObserver methodsFor: 'logOnChange' stamp: 'mk 6/1/2022 20:49:46'!
logOnChange: newHeading
	self subclassResponsibility.! !


!MarsRoverObserver methodsFor: 'update' stamp: 'mk 6/1/2022 20:49:24'!
update
	self subclassResponsibility.! !


!MarsRoverObserver methodsFor: 'initialization' stamp: 'pm 6/1/2022 19:42:51'!
initializeFor: aMarsRover with: aMarsRoverLogger

	marsRover := aMarsRover.
	logger := aMarsRoverLogger.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverObserver class' category: 'MarsRover-WithHeading'!
MarsRoverObserver class
	instanceVariableNames: ''!

!MarsRoverObserver class methodsFor: 'as yet unclassified' stamp: 'pm 6/1/2022 16:52:36'!
for: aMarsRoverLogger

	^self new initializeFor: aMarsRoverLogger.! !

!MarsRoverObserver class methodsFor: 'as yet unclassified' stamp: 'pm 6/1/2022 19:43:39'!
for: aMarsRover with: aMarsRoverLogger

	^self new initializeFor: aMarsRover with: aMarsRoverLogger.! !

!MarsRoverObserver class methodsFor: 'as yet unclassified' stamp: 'pm 5/31/2022 23:08:23'!
heading: aMarsRoverLogger position: anotherMarsRoverLogger

	^self new initializeWithHeading: aMarsRoverLogger andPosition: anotherMarsRoverLogger.! !


!classDefinition: #HeadingObserver category: 'MarsRover-WithHeading'!
MarsRoverObserver subclass: #HeadingObserver
	instanceVariableNames: 'heading'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!HeadingObserver methodsFor: 'update' stamp: 'pm 6/1/2022 20:24:46'!
update

	| newHeading |
	
	newHeading := marsRover heading.
	
	self logOnChange: 	newHeading.
	
	heading := newHeading.! !


!HeadingObserver methodsFor: 'logOnChange' stamp: 'pm 6/1/2022 20:24:19'!
logOnChange: newHeading

	newHeading ~= heading ifTrue: [ logger headingChangedTo: newHeading. ].! !


!HeadingObserver methodsFor: 'initialization' stamp: 'pm 6/1/2022 20:20:44'!
initializeFor: aMarsRover with: aLogger

	super initializeFor: aMarsRover with: aLogger.
	heading := (aMarsRover heading).! !


!classDefinition: #PositionObserver category: 'MarsRover-WithHeading'!
MarsRoverObserver subclass: #PositionObserver
	instanceVariableNames: 'position'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!PositionObserver methodsFor: 'logOnChange' stamp: 'pm 6/1/2022 20:25:35'!
logOnChange: newPosition

	newPosition ~= position ifTrue: [ logger positionChangedTo: newPosition ].! !


!PositionObserver methodsFor: 'initialization' stamp: 'pm 6/1/2022 20:21:18'!
initializeFor: aMarsRover with: aLogger

	super initializeFor: aMarsRover with: aLogger.
	position := (aMarsRover position).! !


!PositionObserver methodsFor: 'update' stamp: 'pm 6/1/2022 20:25:56'!
update

	| newPosition |
	
	newPosition := marsRover position.
	
	self logOnChange: newPosition.
	
	position := newPosition.! !
