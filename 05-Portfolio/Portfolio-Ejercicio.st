!classDefinition: #PortfolioTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #PortfolioTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/16/2022 21:24:37'!
test01EmptyPortfolioHasBalanceEqualsCero

	| portfolio |
	
	portfolio := Portfolio new.
	
	self assert: portfolio balance equals: 0. ! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/18/2022 12:08:57'!
test02PortfolioWithOneAccountHasSameBalanceAsAccount

	| portfolio account |

	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
	
	portfolio := Portfolio including: account.
	
	self assert: portfolio balance equals: 100. ! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/19/2022 20:08:25'!
test03PortfolioWithMultipleAccountsHasBalanceEqualToSumOfAccountsBalances

	| portfolio account anotherAccount |

	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.

	anotherAccount := ReceptiveAccount  new.
	Deposit register: 100 on: anotherAccount.
	
	portfolio := Portfolio including: account.
	portfolio add: anotherAccount.
	
	self assert: portfolio balance equals: 200.  ! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/19/2022 20:08:40'!
test04PortfolioWithOnePortfolioHasSameBalanceAsIncludedPortfolio

	| portfolio account anotherAccount aPortfolio |

	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
	
	anotherAccount := ReceptiveAccount  new.
	Deposit register: 100 on: anotherAccount.
	
	aPortfolio := Portfolio including: account.
	aPortfolio add: anotherAccount.
	
	portfolio := Portfolio new.
	portfolio add: aPortfolio. 
	
	self assert: portfolio balance equals: 200.  ! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/19/2022 20:08:56'!
test05PortfolioWithMultiplePortfoliosHasBalanceEqualToSumOfPortfoliosBalances

	| portfolio account anotherAccount aPortfolio anotherPortfolio |

	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
	
	anotherAccount := ReceptiveAccount  new.
	Deposit register: 50 on: anotherAccount.
	
	aPortfolio := Portfolio including: account.
	anotherPortfolio := Portfolio including: anotherAccount.
	
	portfolio := Portfolio new.
	portfolio add: aPortfolio.
	portfolio add: anotherPortfolio.
	
	self assert: portfolio balance equals: 150.  ! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/18/2022 14:15:56'!
test06EmptyPortfolioHasNoTransactions

	| portfolio |
	
	portfolio := Portfolio new.
	
	self assert: portfolio transactions equals: OrderedCollection new.  ! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/18/2022 14:29:28'!
test07PortfolioWithOneAccountHasSameTransactionsAsAccount

	| portfolio account transactions |
	
	account := ReceptiveAccount new.
	
	transactions := OrderedCollection new.
	transactions add: (Deposit register: 100 on: account).
	transactions add: (Deposit register: 100 on: account).
	
	portfolio := Portfolio including: account.
	
	self assert: portfolio transactions equals: transactions.  ! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/19/2022 20:09:46'!
test08PortfolioWithMultipleAccountsHasSameTransactionsAsAllTheAccountsCombined

	| portfolio account transactions anotherAccount |
	
	account := ReceptiveAccount new.
	anotherAccount := ReceptiveAccount new.
	
	transactions := OrderedCollection new.
	transactions add: (Deposit register: 100 on: account).
	transactions add: (Deposit register: 100 on: account).
	transactions add: (Withdraw register: 50 on: anotherAccount).

	portfolio := Portfolio including: account.
	portfolio add: anotherAccount.
	
	self assert: portfolio transactions equals: transactions.  ! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/19/2022 20:10:00'!
test09PortfolioWithMultiplePortfolioHasSameTransactionsAsAllPortfoliosCombined

	| portfolio account transactions anotherAccount aPortfolio anotherPortfolio |
	
	account := ReceptiveAccount new.
	anotherAccount := ReceptiveAccount new.
	
	transactions := OrderedCollection new.
	transactions add: (Deposit register: 100 on: account).
	transactions add: (Deposit register: 100 on: account).
	transactions add: (Withdraw register: 50 on: anotherAccount).

	aPortfolio := Portfolio including: account.
	anotherPortfolio := Portfolio including: anotherAccount.
	
	portfolio := Portfolio new.
	portfolio add: aPortfolio. 
	portfolio add: anotherPortfolio.
	
	self assert: portfolio transactions equals: transactions.  ! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/18/2022 15:30:53'!
test10EmptyPortfolioHasNoRegisteredTransaction

	| portfolio transaction |
	
	portfolio := Portfolio new.
		
	transaction := Deposit register: 100 on: (ReceptiveAccount new).

	self assert: (portfolio hasRegistered: transaction) equals: false.! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/18/2022 15:30:58'!
test11PortfolioWithAnAccountWithRegisteredTransactionHasThatTransaction

	| portfolio transaction account |
	
	account := ReceptiveAccount new.
	transaction := Deposit register: 100 on: account.
	
	portfolio := Portfolio including: account.

	self assert: (portfolio hasRegistered: transaction) equals: true.! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/18/2022 15:36:47'!
test12PortfolioWithAnAccountWithRegisteredTransactionThatIsNotTheOneWeWant

	| portfolio account transaction |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	
	portfolio := Portfolio including: account.

	transaction := Deposit register: 50 on: (ReceptiveAccount new).
	
	self assert: (portfolio hasRegistered: transaction) equals: false.! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/19/2022 20:11:49'!
test13PortfolioWithMultipleAccountsWithRegisteredTransactionHasThatTransaction

	| portfolio account transaction anotherAccount |
	
	account := ReceptiveAccount new.
	anotherAccount := ReceptiveAccount new.
	
	Deposit register: 100 on: account.
	transaction := Deposit register: 50 on: anotherAccount.
		
	portfolio := Portfolio including: account.
	portfolio add: anotherAccount.
	
	self assert: (portfolio hasRegistered: transaction) equals: true.! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/19/2022 20:11:58'!
test14PortfolioWithOnePorfolioWithRegisteredTransactionHasThatTransaction

	| portfolio account transaction aPortfolio |
	
	account := ReceptiveAccount new.	
	transaction := Deposit register: 50 on: account.
		
	aPortfolio := Portfolio including: account.
	portfolio := Portfolio new add: aPortfolio.
	
	self assert: (portfolio hasRegistered: transaction) equals: true.! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/19/2022 20:12:06'!
test15PortfolioWithOnePorfolioWithRegisteredTransactionThatIsNotTheOneWeWant

	| portfolio account transaction aPortfolio |
	
	account := ReceptiveAccount new.	
	Deposit register: 100 on: account.
	
	transaction := Deposit register: 50 on: (ReceptiveAccount new).
		
	aPortfolio := Portfolio including: account.
	portfolio := Portfolio new add: aPortfolio.
	
	self assert: (portfolio hasRegistered: transaction) equals: false.! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/19/2022 20:12:23'!
test16PortfolioWithMultiplePorfoliosWithRegisteredTransactionThatIsNotTheOneWeWant

	| portfolio account transaction aPortfolio anotherAccount anotherPortfolio |
	
	account := ReceptiveAccount new.	
	transaction := Deposit register: 100 on: account.
	anotherAccount := ReceptiveAccount new.
	
	Deposit register: 50 on: anotherAccount.
		
	aPortfolio := Portfolio including: account.
	anotherPortfolio := Portfolio including: anotherAccount.
	
	portfolio := Portfolio new. 
	portfolio add: aPortfolio.
	portfolio add: anotherPortfolio.
	
	self assert: (portfolio hasRegistered: transaction) equals: true.! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/22/2022 13:44:43'!
test17PortfolioCanAddTwoDifferentAccounts

	| portfolio account anotherAccount |
	
	account := ReceptiveAccount new.	
	anotherAccount := ReceptiveAccount new.
		
	portfolio := Portfolio new. 
	portfolio add: account.
	
	self assert: (portfolio canAdd: anotherAccount) equals: true.! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/22/2022 13:47:49'!
test18PortfolioCanNotAddTheSameAccountTwoTimes

	| portfolio account |
	
	account := ReceptiveAccount new.	
		
	portfolio := Portfolio new. 
	portfolio add: account.
	
	self assert: (portfolio canAdd: account) equals: false.! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/22/2022 19:41:42'!
test19PortfolioCanNotAddPortfolioWithAccountsInCommon

	| portfolio anotherPortfolio account anotheerPortfolio |
	
	account := ReceptiveAccount new.	
		
	portfolio := Portfolio new. 
	portfolio add: account.
	
	anotherPortfolio := Portfolio new.
	anotheerPortfolio := Portfolio new.
	
	anotherPortfolio add: account.  
	anotheerPortfolio add: anotherPortfolio.
	
	self assert: (portfolio canAdd: anotheerPortfolio) equals: false.! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/22/2022 19:37:25'!
test20CanNotAddAccountToPortfolioBelongingToPortfolioIncludingThatAccount

	| portfolio anotherPortfolio account |
	
	account := ReceptiveAccount new.	
	
	portfolio := Portfolio including: account.
	
	anotherPortfolio := Portfolio new.
	
	portfolio add: anotherPortfolio. 
	
	self assert: (anotherPortfolio canAdd: account) equals: false.! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/22/2022 19:43:40'!
test21PortfolioCanNotAddPortfolioWithPortfolioWithAccountsInCommon

	| portfolio anotherPortfolio account someOtherPortfolio |
	
	account := ReceptiveAccount new.	
		
	portfolio := Portfolio new. 
	portfolio add: account.
	
	anotherPortfolio := Portfolio new.
	someOtherPortfolio := Portfolio new.
	
	anotherPortfolio add: account.  
	someOtherPortfolio add: anotherPortfolio.
	
	self assert: (portfolio canAdd: someOtherPortfolio) equals: false.! !

!PortfolioTest methodsFor: 'as yet unclassified' stamp: 'pm 5/22/2022 20:03:27'!
test22RaiseErrorWhenUnableToAddFinancialRegisterToPortfolio

	| portfolio account |
	
	account := ReceptiveAccount new.	
		
	portfolio := Portfolio new. 
	portfolio add: account.
	
	[portfolio add: account] on: Error do: [^Portfolio cannotAddFinancialRegister].! !


!classDefinition: #ReceptiveAccountTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #ReceptiveAccountTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:44'!
test01ReceptiveAccountHaveZeroAsBalanceWhenCreated 

	| account |
	
	account := ReceptiveAccount new.

	self assert: 0 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:48'!
test02DepositIncreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
		
	self assert: 100 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:52'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
		
	self assert: 50 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:32'!
test04WithdrawValueMustBePositive 

	| account withdrawValue |
	
	account := ReceptiveAccount new.
	withdrawValue := 50.
	
	self assert: withdrawValue equals: (Withdraw register: withdrawValue on: account) value
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 5/16/2022 16:18:39'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	withdraw := Withdraw register: 50 on: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 5/16/2022 16:19:11'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| deposit withdraw account |
	
	account := ReceptiveAccount new.
	deposit :=  Deposit for: 200.
	withdraw := Withdraw for: 50.
	
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered:withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:14:01'!
test07AccountKnowsItsTransactions 

	| account1 deposit1 |
	
	account1 := ReceptiveAccount new.
	
	deposit1 := Deposit register: 50 on: account1.
		
	self assert: 1 equals: account1 transactions size.
	self assert: (account1 transactions includes: deposit1).
! !


!classDefinition: #AccountTransaction category: 'Portfolio-Ejercicio'!
Object subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountTransaction methodsFor: 'value' stamp: 'HernanWilkinson 9/12/2011 12:25'!
value 

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: 'Portfolio-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'NR 10/17/2019 03:22:00'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #Deposit category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Deposit methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:45'!
initializeFor: aValue

	value := aValue ! !


!Deposit methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !


!Deposit methodsFor: 'as yet unclassified' stamp: 'pm 5/16/2022 20:55:15'!
contemplateInBalance: aBalance

	^aBalance + self value.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: 'Portfolio-Ejercicio'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #Withdraw category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Withdraw methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:46'!
initializeFor: aValue

	value := aValue ! !


!Withdraw methodsFor: 'value' stamp: 'pm 5/16/2022 21:05:09'!
contemplateInBalance: aBalance

	^aBalance - self value.! !

!Withdraw methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:33'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: 'Portfolio-Ejercicio'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #FinancialRegister category: 'Portfolio-Ejercicio'!
Object subclass: #FinancialRegister
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!FinancialRegister methodsFor: 'transaction' stamp: 'pm 5/19/2022 20:18:45'!
transactions

	self subclassResponsibility.! !


!FinancialRegister methodsFor: 'balance' stamp: 'pm 5/19/2022 20:18:38'!
balance

	self subclassResponsibility.! !


!FinancialRegister methodsFor: 'testing' stamp: 'pm 5/22/2022 20:04:33'!
canAdd: aFinancialRegister

	self subclassResponsibility.! !

!FinancialRegister methodsFor: 'testing' stamp: 'pm 5/22/2022 20:04:41'!
canAddInCurrentTree: aFinancialRegister

	self subclassResponsibility.! !

!FinancialRegister methodsFor: 'testing' stamp: 'pm 5/19/2022 20:18:59'!
hasRegistered: aTransaction

	self subclassResponsibility.! !


!FinancialRegister methodsFor: 'parent' stamp: 'pm 5/22/2022 20:05:40'!
setParentTo: aPortfolio

	self subclassResponsibility.! !


!classDefinition: #NullPortfolio category: 'Portfolio-Ejercicio'!
FinancialRegister subclass: #NullPortfolio
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!NullPortfolio methodsFor: 'as yet unclassified' stamp: 'pm 5/22/2022 14:35:18'!
canAdd: aFinancialRegister

	^true.! !


!classDefinition: #Portfolio category: 'Portfolio-Ejercicio'!
FinancialRegister subclass: #Portfolio
	instanceVariableNames: 'financialRegisters includedIn'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Portfolio methodsFor: 'balance' stamp: 'pm 5/19/2022 21:13:25'!
balance

	^financialRegisters sum: [ :aFinancialRegister | aFinancialRegister balance ] ifEmpty: [ 0 ]! !


!Portfolio methodsFor: 'initialization' stamp: 'pm 5/22/2022 14:35:41'!
initialize

	financialRegisters := OrderedCollection new.
	includedIn := NullPortfolio new.! !

!Portfolio methodsFor: 'initialization' stamp: 'pm 5/19/2022 21:14:08'!
initializeWith: aFinancialRegister

	financialRegisters add: aFinancialRegister.! !


!Portfolio methodsFor: 'transaction' stamp: 'pm 5/19/2022 21:14:44'!
transactions
	| transactions |
	
	transactions := OrderedCollection new.
	financialRegisters do: [:aFinancialRegister | transactions addAll: aFinancialRegister transactions ].
	^transactions.! !


!Portfolio methodsFor: 'add' stamp: 'pm 5/22/2022 19:55:14'!
add: aFinancialRegister
	
	self assertCanAdd: aFinancialRegister.
	
	aFinancialRegister setParentTo: self.
	
	financialRegisters add: aFinancialRegister.! !

!Portfolio methodsFor: 'add' stamp: 'pm 5/22/2022 19:58:46'!
assertCanAdd: aFinancialRegister

	(self canAdd: aFinancialRegister) ifFalse: [ ^self error: Portfolio cannotAddFinancialRegister ].! !


!Portfolio methodsFor: 'testing' stamp: 'pm 5/22/2022 19:48:29'!
canAdd: aFinancialRegister
	
	^(self canAddInParentTree: aFinancialRegister) and:
	 	(self canAddInCurrentTree: aFinancialRegister) and:
		(aFinancialRegister canAddInCurrentTree: self).! !

!Portfolio methodsFor: 'testing' stamp: 'pm 5/22/2022 14:49:02'!
canAddInCurrentTree: aFinancialRegister
	
	^financialRegisters inject: true into:
		 [:canBeAdded :myFinancialRegister | canBeAdded and: (aFinancialRegister canAdd: myFinancialRegister) ]! !

!Portfolio methodsFor: 'testing' stamp: 'pm 5/22/2022 14:48:24'!
canAddInParentTree: aFinancialRegister
	
	^includedIn canAdd: aFinancialRegister.! !

!Portfolio methodsFor: 'testing' stamp: 'pm 5/19/2022 21:15:18'!
hasRegistered: aTransaction

	^financialRegisters anySatisfy: [:aFinancialRegister | aFinancialRegister hasRegistered: aTransaction].! !


!Portfolio methodsFor: 'parent' stamp: 'pm 5/22/2022 14:27:29'!
setParentTo: aPortfolio

	includedIn := aPortfolio.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Portfolio class' category: 'Portfolio-Ejercicio'!
Portfolio class
	instanceVariableNames: ''!

!Portfolio class methodsFor: 'as yet unclassified' stamp: 'pm 5/22/2022 19:56:19'!
cannotAddFinancialRegister

	^'You can not add this financial register to the portfolio!!'! !

!Portfolio class methodsFor: 'as yet unclassified' stamp: 'pm 5/16/2022 21:25:50'!
including: anAccount

	^self new initializeWith: anAccount.! !


!classDefinition: #ReceptiveAccount category: 'Portfolio-Ejercicio'!
FinancialRegister subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'NR 10/17/2019 15:06:56'!
initialize

	transactions := OrderedCollection new.! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !


!ReceptiveAccount methodsFor: 'balance' stamp: 'pm 5/16/2022 21:16:28'!
balance

	^transactions inject: 0 into: [ :balance :aTransaction | aTransaction contemplateInBalance: balance ].! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'pm 5/22/2022 14:10:53'!
canAdd: aReceptiveAccount

	^(self == aReceptiveAccount) not.! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'pm 5/22/2022 19:47:01'!
canAddInCurrentTree: aFinancialRegister

	^true.! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'pm 5/18/2022 15:29:27'!
hasRegistered: aTransaction

	^ transactions includes: aTransaction 
! !


!ReceptiveAccount methodsFor: 'parent' stamp: 'pm 5/22/2022 14:45:59'!
setParentTo: aPortfolio! !
