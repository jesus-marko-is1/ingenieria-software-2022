!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing' stamp: 'pm 4/11/2022 21:26:51'!
assertActionDoesNotExceedsCertainTime: anAction timeLimit: milisecondsLimit
	
	| millisecondsAfterRunning millisecondsBeforeRunning |
	
	millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
	anAction value.
	millisecondsAfterRunning := Time millisecondClockValue * millisecond.
	
	self assert: (millisecondsAfterRunning-millisecondsBeforeRunning) < (milisecondsLimit * millisecond)
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'pm 4/16/2022 12:38:17'!
assertBook: customerBook onlyIncludesCustomerNamed: customerName 
			self assert: customerBook numberOfCustomers = 1.
			self assert: (customerBook includesCustomerNamed: customerName)
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'pm 4/16/2022 13:51:47'!
assertCannotSuspendCustomer: customerName inBook: customerBook withExistingCustomer: existingCustomerName

	[ customerBook suspendCustomerNamed: customerName.
	self fail ]
		on: CantSuspend 
		do: [ :anError | self assertBook: customerBook onlyIncludesCustomerNamed: existingCustomerName ]
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'pm 4/16/2022 13:39:31'!
assertNumberOfCustomersForType: customerBook active: expectedActive suspended: expectedSuspended total: expectedCustomer

    self assert: expectedActive equals: customerBook numberOfActiveCustomers.
    self assert: expectedSuspended equals: customerBook numberOfSuspendedCustomers.
    self assert: expectedCustomer equals: customerBook numberOfCustomers.
	

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'pm 4/11/2022 21:28:26'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds

	| customerBook customerName |
	
	customerBook := CustomerBook new.
	customerName := 'John Lennon'.
	
	self assertActionDoesNotExceedsCertainTime: 
	[customerBook addCustomerNamed: customerName] timeLimit: 50.
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'pm 4/11/2022 21:30:16'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| customerBook customerName |
	
	customerBook := CustomerBook new.
	customerName := 'Paul McCartney'.
	
	customerBook addCustomerNamed: customerName.
	  
	self assertActionDoesNotExceedsCertainTime: 
	[customerBook removeCustomerNamed: customerName] timeLimit: 100.
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'HernanWilkinson 5/9/2012 18:12'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook |
			
	customerBook := CustomerBook new.

	[ customerBook addCustomerNamed: ''.
	self fail ]
		on: Error 
		do: [ :anError | 
			self assert: anError messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
			self assert: customerBook isEmpty ]! !

!CustomerBookTest methodsFor: 'testing' stamp: 'pm 4/16/2022 13:45:39'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	[ customerBook removeCustomerNamed: 'Paul McCartney'.
	self fail ]
		on: NotFound 
		do: [ :anError | self assertBook: customerBook onlyIncludesCustomerNamed: johnLennon ]
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'pm 4/16/2022 13:42:51'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	
	self assertNumberOfCustomersForType: customerBook active: 0 suspended: 1 total: 1.
	self assert: (customerBook includesCustomerNamed: paulMcCartney).
	

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'pm 4/16/2022 13:42:16'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	customerBook removeCustomerNamed: paulMcCartney.
	
	self assertNumberOfCustomersForType: customerBook active: 0 suspended: 0 total: 0.
	self deny: (customerBook includesCustomerNamed: paulMcCartney).


	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'pm 4/16/2022 13:53:47'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self assertCannotSuspendCustomer: 'George Harrison' inBook: customerBook withExistingCustomer: johnLennon
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'pm 4/16/2022 13:52:51'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	customerBook suspendCustomerNamed: johnLennon.
	
	self assertCannotSuspendCustomer: johnLennon inBook: customerBook withExistingCustomer: johnLennon! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
includesCustomerNamed: aName

	^(active includes: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 9/17/2020 07:23:04'!
initialize

	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'pm 4/16/2022 14:12:28'!
addCustomerNamed: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	(self includesCustomerNamed: aName) ifTrue: [ self signalCustomerAlreadyExists ].
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfCustomers
	
	^active size + suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'pm 4/16/2022 16:10:29'!
removeCustomerNamed: aName 
 
	((self removeCustomerNamed: aName fromCollection: active) = aName) ifTrue: [^aName].
	((self removeCustomerNamed: aName fromCollection: suspended) = aName) ifTrue: [^aName].
	
	^ NotFound signal.
! !

!CustomerBook methodsFor: 'customer management' stamp: 'pm 4/16/2022 16:08:48'!
removeCustomerNamed: customerName fromCollection: customersCollection
	
	1 to: customersCollection size do: 	
	[ :index |
		customerName = (customersCollection at: index)
			ifTrue: [
				customersCollection removeAt: index.
				^ customerName 
			] 
	].
! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'pm 4/16/2022 16:26:30'!
suspendCustomerNamed: aName 
	
	((self removeCustomerNamed: aName fromCollection: active) = aName) ifFalse: [^CantSuspend signal].
	suspended add: aName
	
	
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/11/2022 07:18:12'!
customerAlreadyExistsErrorMessage

	^'Customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/11/2022 07:18:16'!
customerCanNotBeEmptyErrorMessage

	^'Customer name cannot be empty!!!!!!'! !
