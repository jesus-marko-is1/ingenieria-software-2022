!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'as yet unclassified' stamp: 'pm 6/2/2022 20:44:56'!
test01newCartIsEmpty

	| cart |
	
	cart := Cart new.
	
	self assert: cart isEmpty equals: true.! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'pm 6/5/2022 19:34:34'!
test02cartWithBookIsNotEmpty

	| cart book catalog |
	
	book := '1234567891021'.
	catalog := BookCatalog new.
	catalog registerBook: book withPrice: 50.
	
	cart := Cart from: catalog.
	
	cart add: 	book amount: 1.
	
	self assert: cart isEmpty equals: false.! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'pm 6/5/2022 19:28:28'!
test03addTwoBooksOfTheSameTitleToCart

	| cart book catalog |
	
	book := '1234567891021'.
	catalog := BookCatalog new.
	catalog registerBook: book withPrice: 50.
	
	cart := Cart from: catalog.
	
	cart add: book amount: 2.
		
	self assert: (cart hasAmountOf: book) equals: 2.! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'pm 6/5/2022 19:28:34'!
test04removeBookFromCart

	| cart book catalog |
	
	book := '1234567891021'.
	catalog := BookCatalog new.
	catalog registerBook: book withPrice: 50.
	
	cart := Cart from: catalog.
	
	cart add: book amount: 3.
	
	cart remove: book amount: 2.
		
	self assert: (cart hasAmountOf: book) equals: 1.! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'pm 6/5/2022 19:28:42'!
test05CannotRemoveBookFromCartIfNotPresent

	| cart book catalog |
	
	book := '1234567891021'.
	catalog := BookCatalog new.
	catalog registerBook: book withPrice: 50.
	
	cart := Cart from: catalog.
	
	cart add: book amount: 2.
			
	self 
		should: [cart remove: book amount: 4]
		raise: Error
		withExceptionDo: [ 
			:anError | 
				self assert: anError messageText equals: Cart bookNotPresent.
				self assert: (cart hasAmountOf: book) equals: 0 
		].! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'pm 6/5/2022 19:28:54'!
test06CannotAddBookThatsNotOffered

	| cart catalog invalidBook book |
	
	book := '1234567891021'.
	catalog := BookCatalog new.
	catalog registerBook: book withPrice: 50.
	
	cart := Cart from: catalog.
	
	invalidBook := '1234567891022'.
			
	self 
		should: [cart add: invalidBook amount: 1]
		raise: Error
		withExceptionDo: [ 
			:anError | 
				self assert: anError messageText equals: Cart bookNotOffered.
				self assert: (cart hasAmountOf: invalidBook) equals: 0 
		].! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'pm 6/5/2022 19:29:04'!
test07cartCalculateFinalPriceAmount

	| cart catalog book anotherBook |
	
	book := '1234567891021'.
	anotherBook := '1234567891022'.
	catalog := BookCatalog new.
	catalog registerBook: book withPrice: 50.
	catalog registerBook: anotherBook withPrice: 100.
	
	cart := Cart from: catalog.
	
	cart add: book amount: 2.
	cart add: anotherBook amount: 3.
			
	self assert: cart finalPrice equals: 400! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'pm 6/5/2022 19:31:45'!
test08CannotRegisterBookWithInvalidISBN
	
	| catalog |
	catalog := BookCatalog new. 

	self 
		should: [ catalog registerBook: '2' withPrice: 50 ]
		raise: Error
		withExceptionDo: [ 
			:anError | 
				self assert: anError messageText equals: BookCatalog InvalidISBN. 
		].! !


!classDefinition: #BookCatalog category: 'TusLibros'!
Object subclass: #BookCatalog
	instanceVariableNames: 'catalog'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!BookCatalog methodsFor: 'register' stamp: 'pm 6/5/2022 19:33:20'!
registerBook: aBookISBN withPrice: aPrice

	self assertISBNIsValid: aBookISBN.
	
	catalog at: aBookISBN put: aPrice.! !


!BookCatalog methodsFor: 'initialization' stamp: 'pm 6/4/2022 19:19:30'!
initialize

	catalog := Dictionary new.! !


!BookCatalog methodsFor: 'testing' stamp: 'pm 6/5/2022 19:33:41'!
isRegistered: aBookISBN

	^catalog includesKey: aBookISBN! !


!BookCatalog methodsFor: 'price' stamp: 'pm 6/5/2022 19:33:52'!
priceOf: aBookISBN

	^catalog at: aBookISBN.! !


!BookCatalog methodsFor: 'error' stamp: 'pm 6/5/2022 19:26:59'!
assertISBNIsValid: anISBN

	^ anISBN size ~= 13 ifTrue:[self raiseInvalidISBNError]! !

!BookCatalog methodsFor: 'error' stamp: 'pm 6/5/2022 19:31:38'!
raiseInvalidISBNError

	^ self error: BookCatalog InvalidISBN! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'BookCatalog class' category: 'TusLibros'!
BookCatalog class
	instanceVariableNames: ''!

!BookCatalog class methodsFor: 'error' stamp: 'pm 6/5/2022 19:32:10'!
InvalidISBN
	^'The ISBN is invalid'! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'books catalog'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'testing' stamp: 'pm 6/2/2022 20:49:34'!
isEmpty
	
	^books isEmpty! !


!Cart methodsFor: 'initialization' stamp: 'pm 6/2/2022 21:34:10'!
initialize

	books := Bag new.! !

!Cart methodsFor: 'initialization' stamp: 'pm 6/4/2022 18:41:03'!
initializeWith: aBookCatalog

	self initialize.
	catalog := aBookCatalog.	! !


!Cart methodsFor: 'add' stamp: 'pm 6/5/2022 19:34:53'!
add: aBookISBN amount: anAmount.
	
	self assertBookRegisteredInCatalog: aBookISBN.
	books add: aBookISBN withOccurrences: anAmount.! !


!Cart methodsFor: 'amount' stamp: 'pm 6/5/2022 19:35:37'!
hasAmountOf: aBookISBN

	^books occurrencesOf: aBookISBN
	! !


!Cart methodsFor: 'remove' stamp: 'pm 6/5/2022 19:35:56'!
remove: aBookISBN amount: anAmount

	anAmount timesRepeat: [ books remove: aBookISBN ifAbsent: [ self raiseBookNotPresentError ] ].
	
	! !


!Cart methodsFor: 'error' stamp: 'pm 6/5/2022 19:35:13'!
assertBookRegisteredInCatalog: aBookISBN

	(catalog isRegistered: aBookISBN) ifFalse: [^self raiseBookNotRegisteredError]! !

!Cart methodsFor: 'error' stamp: 'pm 6/4/2022 18:19:48'!
raiseBookNotPresentError

	^self error: Cart bookNotPresent! !

!Cart methodsFor: 'error' stamp: 'pm 6/4/2022 18:45:10'!
raiseBookNotRegisteredError

	^self error: Cart bookNotOffered.! !


!Cart methodsFor: 'price' stamp: 'pm 6/5/2022 19:35:25'!
finalPrice
	
	^books sum: [:bookISBN | catalog priceOf: bookISBN ].
	
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'error' stamp: 'pm 6/4/2022 18:39:23'!
bookNotOffered
	
	^'Book is not offered at this moment'.! !

!Cart class methodsFor: 'error' stamp: 'pm 6/4/2022 18:17:38'!
bookNotPresent
	
	^'Book is not included in Cart'.! !


!Cart class methodsFor: 'initialization' stamp: 'pm 6/4/2022 18:40:01'!
from: aBookCatalog 
	
	^self new initializeWith: aBookCatalog.! !
