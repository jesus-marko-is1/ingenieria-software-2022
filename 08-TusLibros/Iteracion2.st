!classDefinition: #TusLibrosTest category: 'TusLibros'!
TestCase subclass: #TusLibrosTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosTest methodsFor: 'support' stamp: 'om 6/8/2022 20:18:27'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog! !

!TusLibrosTest methodsFor: 'support' stamp: 'om 6/8/2022 20:18:42'!
defaultCatalog
	
	| catalog |
	
	catalog := Dictionary new. 
	catalog add: (self itemSellByTheStore)->50.
	^catalog.! !

!TusLibrosTest methodsFor: 'support' stamp: 'om 6/8/2022 20:18:59'!
itemSellByTheStore
	
	^ 'validBook'! !


!classDefinition: #CartTest category: 'TusLibros'!
TusLibrosTest subclass: #CartTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:08'!
test01NewCartsAreCreatedEmpty

	self assert: self createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [ cart add: self itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := self createCart.
	
	cart add: self itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 0 of: self itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 2 of: self itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test06CartRemembersAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: self itemSellByTheStore.
	self assert: (cart includes: self itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := self createCart.
	
	self deny: (cart includes: self itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: 2 of: self itemSellByTheStore.
	self assert: (cart occurrencesOf: self itemSellByTheStore) = 2! !


!CartTest methodsFor: 'support' stamp: 'HernanWilkinson 6/17/2013 17:44'!
itemNotSellByTheStore
	
	^'invalidBook'! !


!classDefinition: #CashierTest category: 'TusLibros'!
TusLibrosTest subclass: #CashierTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'om 6/8/2022 20:11:26'!
test01checkoutOnEmptyCartRaisesError

	| cart cashier salesBook merchantProcessor |
	
	cart := self createCart.
	merchantProcessor := MerchantProcessorDouble new.
	salesBook := Dictionary new.
	cashier := Cashier withSalesBook: salesBook paymentService: merchantProcessor.
	
	self 
		should: [cashier checkout: cart payingWith: (self defaultCreditCard)]
		raise: Error
		withExceptionDo: [
			:anError | self assert: anError messageText equals: Cashier cartCannotBeEmpty.
			salesBook isEmpty
		].
		! !

!CashierTest methodsFor: 'tests' stamp: 'om 6/8/2022 20:11:52'!
test02checkoutOfCartWithOneItemsReturnsCorrespondingPrice

	| cart cashier creditCard salesBook amount merchantProcessor |
	
	cart := self createCart.
	cart add: self itemSellByTheStore.
	
	merchantProcessor := MerchantProcessorDouble new.
	salesBook := Dictionary new.
	cashier := Cashier withSalesBook: salesBook paymentService: merchantProcessor.
	
	creditCard := self defaultCreditCard.
	
	amount := 50.
	
	self assert: (cashier checkout: cart payingWith: creditCard) equals: amount.
	self assert: (salesBook at: creditCard) equals: amount.! !

!CashierTest methodsFor: 'tests' stamp: 'om 6/8/2022 20:12:40'!
test03checkoutOfCartWithManyItemsReturnsCorrespondingPrice

	| cart cashier creditCard salesBook amount merchantProcessor |
	
	cart := self createCart.
	cart add: 3 of: self itemSellByTheStore.
	
	merchantProcessor := MerchantProcessorDouble new.
	salesBook := Dictionary new.
	cashier := Cashier withSalesBook: salesBook paymentService: merchantProcessor.
	
	creditCard := self defaultCreditCard.
	
	amount := 150.
	
	self assert: (cashier checkout: cart payingWith: creditCard) equals: amount.
	self assert: (salesBook at: creditCard) equals: amount.! !

!CashierTest methodsFor: 'tests' stamp: 'om 6/8/2022 20:23:53'!
test04cannotCheckoutWithExpiredCreditCard

	| cart cashier creditCard salesBook merchantProcessor |
	
	cart := self createCart.
	cart add: 3 of: self itemSellByTheStore.
	
	merchantProcessor := MerchantProcessorDouble expectError: 'Credit card is expired'.
	salesBook := Dictionary new.
	cashier := Cashier withSalesBook: salesBook paymentService: merchantProcessor.
	
	creditCard := self expiredCreditCard.
	
	self 
		should: [cashier checkout: cart payingWith: creditCard]
		raise: Error 
		withExceptionDo: [
			:anError | self assert: anError messageText equals: Cashier invalidCreditCard.
			self assert: salesBook isEmpty.
		].! !

!CashierTest methodsFor: 'tests' stamp: 'om 6/8/2022 20:13:57'!
test05cannotCheckoutWithInsufficientFunds
	| cart cashier creditCard salesBook merchantProcessor errorMessage |
	
	cart := self createCart.
	cart add: 3 of: self itemSellByTheStore.
	
	errorMessage := 'Insufficient funds for given credit card'.
	merchantProcessor := MerchantProcessorDouble expectError: errorMessage.
	salesBook := Dictionary new.
	cashier := Cashier withSalesBook: salesBook paymentService: merchantProcessor.
	
	creditCard := self defaultCreditCard.
	
	self 
		should: [cashier checkout: cart payingWith: creditCard]
		raise: Error 
		withExceptionDo: [
			:anError | self assert: anError messageText equals: '1|', errorMessage.
			self assert: salesBook isEmpty.
		]! !


!CashierTest methodsFor: 'suppport' stamp: 'om 6/8/2022 20:17:16'!
defaultCreditCard

	^CreditCard number: '4212123412341234' name: 'Juan' expiration: ((Date newDay: 1 month: 12  year: 2023) month).! !

!CashierTest methodsFor: 'suppport' stamp: 'om 6/8/2022 20:17:41'!
expiredCreditCard

	^CreditCard 
		number: '4212123412341234'
		name: 'Juan'
		expiration: ((Date newDay: 1 month: 12  year: 2021) month).! !


!classDefinition: #CreditCardTest category: 'TusLibros'!
TusLibrosTest subclass: #CreditCardTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCardTest methodsFor: 'tests' stamp: 'pm 6/7/2022 21:53:03'!
test01cannotCreateCreditCardWithInvalidNumberLength

	
	| creditCard |
	
	self
		should: [creditCard := CreditCard number: '42' name: 'Samuel' expiration: Date today month ]
		raise: Error
		withExceptionDo: [
			:anError | self assert: anError messageText equals: CreditCard numberInvalidError.
			self assert: creditCard isNil. 
		]		! !

!CreditCardTest methodsFor: 'tests' stamp: 'pm 6/7/2022 21:56:23'!
test02cannotCreateCreditCardWithInvalidNumberDigits
	
	| creditCard |
	
	self
		should: [creditCard := CreditCard number: '421212324321123a' name: 'Pedro' expiration: Date today month ]
		raise: Error
		withExceptionDo: [
			:anError | self assert: anError messageText equals: CreditCard numberInvalidError.
			self assert: creditCard isNil. 
		]		! !

!CreditCardTest methodsFor: 'tests' stamp: 'pm 6/7/2022 22:04:35'!
test03cannotCreateCreditCardWithInvalidName
	
	| creditCard |
	
	self
		should: [creditCard := CreditCard number: '4212123243211234' name: '  ' expiration: Date today month ]
		raise: Error
		withExceptionDo: [
			:anError | self assert: anError messageText equals: CreditCard nameInvalidError.
			self assert: creditCard isNil. 
		].
	
	self
		should: [creditCard := CreditCard 
			number: '4212123243211234' 
			name: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
			expiration: Date today month ]
		raise: Error
		withExceptionDo: [
			:anError | self assert: anError messageText equals: CreditCard nameInvalidError.
			self assert: creditCard isNil. 
		]		! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'pm 6/6/2022 19:16:21'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 17:48'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'om 6/8/2022 20:08:16'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	aQuantity timesRepeat: [ items add: anItem ]! !


!Cart methodsFor: 'price' stamp: 'pm 6/7/2022 22:09:02'!
finalPrice

	^items sum: [:anItem | self priceOf: anItem]! !

!Cart methodsFor: 'price' stamp: 'pm 6/6/2022 19:23:03'!
priceOf: anItem

	^catalog at: anItem.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'salesBook paymentService'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'checkout' stamp: 'om 6/8/2022 20:00:05'!
checkout: aCart payingWith: aCreditCard

	| finalPrice |
	
	self assertCartIsNotEmpty: aCart.
	self assertCreditCardIsNotExpired: aCreditCard. 
	finalPrice := aCart finalPrice.
	paymentService debit: finalPrice from: aCreditCard.
	self registerSell: finalPrice payedWith: aCreditCard.
	
	^finalPrice! !


!Cashier methodsFor: 'error' stamp: 'pm 6/6/2022 21:39:57'!
assertCartIsNotEmpty: aCart
	
	aCart isEmpty ifTrue: [ ^self raiseCannotCheckoutEmptyCart ].
! !

!Cashier methodsFor: 'error' stamp: 'pm 6/7/2022 21:27:44'!
assertCreditCardIsNotExpired: aCreditCard

	(aCreditCard isExpiredOn: (Date today)) ifTrue: [ ^self raiseCreditCardExpiredError ].! !

!Cashier methodsFor: 'error' stamp: 'pm 6/6/2022 19:06:52'!
raiseCannotCheckoutEmptyCart

	^self error: Cashier cartCannotBeEmpty.! !

!Cashier methodsFor: 'error' stamp: 'pm 6/6/2022 19:56:24'!
raiseCreditCardExpiredError

	^self error: Cashier invalidCreditCard.! !


!Cashier methodsFor: 'salesBook' stamp: 'om 6/8/2022 18:57:33'!
registerSell: anAmount payedWith: aCreditCard

	salesBook ifNotNil: [salesBook at: aCreditCard put: anAmount].! !


!Cashier methodsFor: 'initialization' stamp: 'om 6/8/2022 19:47:07'!
initializeWithSalesBook: aSalesBook paymentService: aPaymentService

	salesBook := aSalesBook.
	paymentService := aPaymentService.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'error' stamp: 'pm 6/6/2022 19:05:20'!
cartCannotBeEmpty
	
	^'Cart must have at least one item for checkout'.! !

!Cashier class methodsFor: 'error' stamp: 'pm 6/6/2022 19:56:53'!
invalidCreditCard
	
	^'Specified credit card is not valid'.! !


!Cashier class methodsFor: 'initialization' stamp: 'om 6/8/2022 19:46:31'!
withSalesBook: aSalesBook paymentService: aPaymentService

	^self new initializeWithSalesBook: aSalesBook paymentService: aPaymentService.! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'number name expires'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'initialization' stamp: 'pm 6/7/2022 21:46:25'!
initializeWithNumber: aCreditCardNumber name: aName expiration: aMonthAndYear.
	
	number := aCreditCardNumber.
	name := aName.
	expires := aMonthAndYear.! !


!CreditCard methodsFor: 'expires' stamp: 'pm 6/7/2022 21:44:41'!
isExpiredOn: aDate
	
	^((Date newDay: 1 month: expires monthIndex year: expires yearNumber) < aDate).! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'initialization' stamp: 'pm 6/7/2022 21:47:10'!
number: aCreditCardNumber name: aName expiration: aMonthAndYear

	self assertNumberIsValid: aCreditCardNumber.
	self assertNameIsValid: aName.
	^self new initializeWithNumber: aCreditCardNumber name: aName expiration: aMonthAndYear.! !


!CreditCard class methodsFor: 'error' stamp: 'pm 6/7/2022 22:00:36'!
assertNameIsValid: aName
	
	(aName size between: 1 and: 30) ifFalse: [ self raiseInvalidName ].
	(self isNotAllSpaces: aName) ifFalse: [ self raiseInvalidName ].! !

!CreditCard class methodsFor: 'error' stamp: 'pm 6/7/2022 21:07:49'!
assertNumberIsValid: aCreditCardNumber

	(self isAllDigits: aCreditCardNumber) ifFalse: [ self raiseInvalidNumber ].
	aCreditCardNumber size ~= 16 ifTrue: [ self raiseInvalidNumber ].! !

!CreditCard class methodsFor: 'error' stamp: 'pm 6/7/2022 21:05:56'!
isAllDigits: aCreditCardNumber

	^ aCreditCardNumber inject: true into: [ :stillValid :aDigit | stillValid and: aDigit isDigit ]! !

!CreditCard class methodsFor: 'error' stamp: 'pm 6/7/2022 21:23:24'!
isNotAllSpaces: aName

	^ aName inject: false into: [ :stillValid :aChar | stillValid or: aChar isAlphaNumeric ]! !

!CreditCard class methodsFor: 'error' stamp: 'pm 6/7/2022 21:24:30'!
nameInvalidError
	
	^'Must specify valid credit card name'.! !

!CreditCard class methodsFor: 'error' stamp: 'pm 6/6/2022 21:55:23'!
numberInvalidError
	
	^'Must specify valid credit card number'.! !

!CreditCard class methodsFor: 'error' stamp: 'pm 6/7/2022 21:24:13'!
raiseInvalidName

	^self error: CreditCard nameInvalidError. ! !

!CreditCard class methodsFor: 'error' stamp: 'pm 6/7/2022 20:46:18'!
raiseInvalidNumber

	^self error: CreditCard numberInvalidError. ! !


!classDefinition: #MerchantProcessorDouble category: 'TusLibros'!
Object subclass: #MerchantProcessorDouble
	instanceVariableNames: 'errorDescription'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessorDouble methodsFor: 'debit' stamp: 'om 6/8/2022 19:54:25'!
debit: anAmount from: aCreditCard

	errorDescription ifNotNil: [^self raisePaymentDidNotSucceed]
	ifNil: [^self returnPaymentSucceeded].! !


!MerchantProcessorDouble methodsFor: 'initialization' stamp: 'om 6/8/2022 19:37:27'!
setForError: anErrorDescription.

	errorDescription := anErrorDescription.! !


!MerchantProcessorDouble methodsFor: 'responses' stamp: 'om 6/8/2022 19:54:03'!
paymentDidNotSucceedError

	^'1|', errorDescription.! !

!MerchantProcessorDouble methodsFor: 'responses' stamp: 'om 6/8/2022 19:55:08'!
raisePaymentDidNotSucceed

	^self error: self paymentDidNotSucceedError.! !

!MerchantProcessorDouble methodsFor: 'responses' stamp: 'om 6/8/2022 19:32:50'!
returnPaymentSucceeded

	^'0|OK'.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MerchantProcessorDouble class' category: 'TusLibros'!
MerchantProcessorDouble class
	instanceVariableNames: ''!

!MerchantProcessorDouble class methodsFor: 'intialization' stamp: 'om 6/8/2022 19:36:28'!
expectError: anErrorDescription

	^self new setForError: anErrorDescription.! !
