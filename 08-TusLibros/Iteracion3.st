!classDefinition: #CartControllerTest category: 'TusLibros'!
TestCase subclass: #CartControllerTest
	instanceVariableNames: 'testObjectsFactory debitBehavior'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/11/2022 16:32:29'!
debit: anAmount from: aCreditCard 

	^debitBehavior value: anAmount value: aCreditCard ! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/11/2022 16:28:15'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.
	debitBehavior := [ :anAmount :aCreditCard | ]! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/11/2022 17:27:17'!
test01createCartWithInvalidClientRaisesAuthenticationError

	| cartController clientID clientPassword authenticator catalog |
	
	authenticator := AuthenticatorDouble expectUnauthorized.
	catalog := Dictionary new.
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	clientID := 2.
	clientPassword := 'password'. 
	
	self
		should:	 [cartController createCartFor: clientID withPassword: clientPassword]
		raise: Error
		withExceptionDo: [ 
			:anError | self assert: anError messageText equals: CartController unauthorizedClientError 
		].
	
	! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/11/2022 17:26:59'!
test02createCartWithValidClientRegistersCartAndReturnsItsIDAlt

	| cartController clientID clientPassword authenticator cartID expectedCartID catalog |
	
	authenticator := AuthenticatorDouble expectAuthorized.
	catalog := Dictionary new.
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	clientID := 1.
	clientPassword := 'password'.
	expectedCartID := 1.
	
	cartID := cartController createCartFor: clientID withPassword: clientPassword.
	
	self assert: cartID equals: expectedCartID.
	self assert: (cartController listCart: cartID) isEmpty.
	
	! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/11/2022 17:26:31'!
test03createCartMultipleTimesWithValidClientRegistersCartsWithDifferentIDs

	| cartController clientID clientPassword authenticator firstCartID secondCartID catalog |
	
	authenticator := AuthenticatorDouble expectAuthorized.
	catalog := Dictionary new.
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	clientID := 1.
	clientPassword := 'password'.
		
	firstCartID := cartController createCartFor: clientID withPassword: clientPassword.
	secondCartID := cartController createCartFor: clientID withPassword: clientPassword.
	
	self deny: (firstCartID == secondCartID).
	self assert: (cartController listCart: firstCartID) isEmpty.
	self assert: (cartController listCart: secondCartID) isEmpty.
	! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/11/2022 17:25:55'!
test04addToCartAddsSpecifiedQuantityOfBookToGivenCart

	| cartController cartID quantity bookISBN authenticator clientID clientPassword catalog |
	
	authenticator := AuthenticatorDouble expectAuthorized.
	bookISBN := '1231231231231'.
	catalog := Dictionary new.
	catalog at: bookISBN put: 50.
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	clientID := 1.
	clientPassword := 'password'.
	quantity := 3.
	
	cartID := cartController createCartFor: clientID withPassword: clientPassword.
	cartController addToCart: cartID book: bookISBN quantity: quantity.
	
	self assert: ((cartController listCart: cartID) occurrencesOf: bookISBN) equals: quantity.
	
	! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/11/2022 17:25:36'!
test05cannotAddToCartToNonExistingCart

	| cartController cartID quantity bookISBN authenticator catalog |
	
	authenticator := AuthenticatorDouble expectAuthorized.
	bookISBN := '1231231231231'.
	catalog := Dictionary new.
	catalog at: bookISBN put: 50.
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	quantity := 3.
	
	cartID := 1.
	
	self should: [cartController addToCart: cartID book: bookISBN quantity: quantity]
		raise: Error
		withExceptionDo: [:anError | self assert: anError messageText equals: CartRegister cartNotFoundError ].
	! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/11/2022 17:25:20'!
test06addToCartBookNotOfferedByCatalogRaisesError

	| cartController cartID quantity bookISBN authenticator clientID clientPassword catalog |
	
	authenticator := AuthenticatorDouble expectAuthorized.
	bookISBN := '1231231231231'.
	catalog := Dictionary new.
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	clientID := 1.
	clientPassword := 'password'.
	quantity := 3.
	
	cartID := cartController createCartFor: clientID withPassword: clientPassword.
	self should: [cartController addToCart: cartID book: bookISBN quantity: quantity]
		raise: Error
		withExceptionDo: [
			:anError | self assert: anError messageText equals: Cart new invalidItemErrorMessage.
			self assert: (cartController listCart: cartID) isEmpty
		].
	! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/11/2022 17:25:01'!
test07listCartOfNonExistingCartRaisesError

	| cartController cartID authenticator catalog |
	
	authenticator := AuthenticatorDouble expectAuthorized.
	catalog := Dictionary new.
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	cartID := 1.
	
	self 
		should: [cartController listCart: cartID]
		raise: Error
		withExceptionDo: [:anError |  self assert: anError messageText equals: CartRegister cartNotFoundError ].! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/11/2022 17:20:42'!
test08cannotCheckoutEmptyCart

	| cartController cartID authenticator catalog clientID clientPassword |
	
	authenticator := AuthenticatorDouble expectAuthorized.
	catalog := Dictionary new.
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	clientID := 1.
	clientPassword := 'password'.
	
	cartID := cartController createCartFor: clientID withPassword: clientPassword.
	
	self 
		should: [ cartController checkout: cartID
							by: clientID
							usingCreditCard: (testObjectsFactory notExpiredCreditCard)
							debitWith: self
		]
		raise: Error
		withExceptionDo: [
			:anError |  self assert: anError messageText equals: Cashier cartCanNotBeEmptyErrorMessage
		].! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/12/2022 19:06:33'!
test09canCheckoutCart

	| cartController cartID authenticator catalog clientID clientPassword bookISBN quantity expectedTransactionID |
	
	authenticator := AuthenticatorDouble expectAuthorized.
	
	bookISBN := '1231231231231'.
	catalog := Dictionary new.
	catalog at: bookISBN put: 50.
	
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	clientID := 1.
	clientPassword := 'password'.
	quantity := 3.
	
	cartID := cartController createCartFor: clientID withPassword: clientPassword.
	cartController addToCart: cartID book: bookISBN quantity: quantity.
	
	expectedTransactionID := 1.
	
	self assert: (
		cartController checkout: cartID
		by: clientID
		usingCreditCard: (testObjectsFactory notExpiredCreditCard)
		debitWith: self
	) equals: expectedTransactionID.
	
	self assert: 
		(cartController listPurchasesFor: clientID usingPassword: clientPassword) last 
		equals: (Sale of: 150 
				     withItems: (OrderedCollection with: bookISBN with: bookISBN with: bookISBN)).! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/12/2022 19:13:01'!
test10cannotCheckoutNonExistingCart

	| cartController cartID authenticator catalog clientID |
	
	authenticator := AuthenticatorDouble expectAuthorized.
	catalog := Dictionary new.
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	clientID := 1.
	cartID := 1.
	
	self 
		should: [ cartController checkout: cartID
							by: clientID
							usingCreditCard: (testObjectsFactory notExpiredCreditCard)
							debitWith: self
		]
		raise: Error
		withExceptionDo: [
			:anError | self assert: anError messageText equals: CartRegister cartNotFoundError
		].! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/12/2022 19:17:42'!
test11cannotCheckoutCartWithExpiredCreditCard

	| cartController cartID authenticator catalog clientID clientPassword bookISBN quantity |
	
	authenticator := AuthenticatorDouble expectAuthorized.
	
	bookISBN := '1231231231231'.
	catalog := Dictionary new.
	catalog at: bookISBN put: 50.
	
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	clientID := 1.
	clientPassword := 'password'.
	quantity := 3.
	
	cartID := cartController createCartFor: clientID withPassword: clientPassword.
	cartController addToCart: cartID book: bookISBN quantity: quantity.
	
	self 
		should: [ cartController checkout: cartID
							by: clientID
							usingCreditCard: (testObjectsFactory expiredCreditCard)
							debitWith: self
		]
		raise: Error
		withExceptionDo: [
			:anError |  self assert: anError messageText equals: 
					Cashier canNotChargeAnExpiredCreditCardErrorMessage 
		].! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'pm 6/12/2022 19:24:24'!
test12cannotListPurchasesOfInvalidCustomer

	| cartController cartID authenticator catalog clientID clientPassword bookISBN quantity |
	
	authenticator := AuthenticatorDouble expectAuthorized.
	
	bookISBN := '1231231231231'.
	catalog := Dictionary new.
	catalog at: bookISBN put: 50.
	
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	clientID := 1.
	clientPassword := 'password'.
	quantity := 3.
	
	cartID := cartController createCartFor: clientID withPassword: clientPassword.
	cartController addToCart: cartID book: bookISBN quantity: quantity.
	
	cartController checkout: cartID
		by: clientID
		usingCreditCard: (testObjectsFactory notExpiredCreditCard)
		debitWith: self.
	
	authenticator := AuthenticatorDouble expectUnauthorized.
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	self should: [cartController listPurchasesFor: clientID usingPassword: clientPassword]
		raise: Error
		withExceptionDo: [
			:anError | self assert: anError messageText equals: CartController unauthorizedClientError
		 ].! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'om 6/12/2022 21:13:03'!
test13cannotListCartWithSessionExpired

	| cartController cartID authenticator catalog clientID clientPassword timestamp |
	
	authenticator := AuthenticatorDouble expectAuthorized.
	catalog := Dictionary new.
	
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	clientID := 1.
	clientPassword := 'password'.
	
	timestamp := (Time localMillisecondClock) - (31 * 60000).
	cartID := cartController createCartFor: clientID withPassword: clientPassword at: timestamp.
	
	self should: [cartController listCart: cartID]
		raise: Error
		withExceptionDo: [
			:anError | self assert: anError messageText equals: CartController cartSessionExpiredError.
		 ].! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'om 6/12/2022 21:15:19'!
test14cannotAddToCartWithSessionExpired

	| cartController cartID authenticator catalog clientID clientPassword timestamp bookISBN quantity |
	
	authenticator := AuthenticatorDouble expectAuthorized.
	bookISBN := '1231231231231'.
	quantity := 3.
	catalog := Dictionary new.
	catalog at: bookISBN put: 50.
		
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	clientID := 1.
	clientPassword := 'password'.
	
	timestamp := (Time localMillisecondClock) - (31 * 60000).
	cartID := cartController createCartFor: clientID withPassword: clientPassword at: timestamp.
	
	self should: [cartController addToCart: cartID book: bookISBN quantity: quantity]
		raise: Error
		withExceptionDo: [
			:anError | self assert: anError messageText equals: CartController cartSessionExpiredError.
		 ].! !

!CartControllerTest methodsFor: 'as yet unclassified' stamp: 'om 6/12/2022 21:27:07'!
test15cannotCheckoutCartWithSessionExpired

	| cartController cartID authenticator catalog clientID clientPassword timestamp |
	
	authenticator := AuthenticatorDouble expectAuthorized.
	
	catalog := Dictionary new.
		
	cartController := CartController withAccessManagedBy: authenticator usingCatalog: catalog.
	
	clientID := 1.
	clientPassword := 'password'.
	
	timestamp := (Time localMillisecondClock) - (31 * 60000).
	cartID := cartController createCartFor: clientID withPassword: clientPassword at: timestamp.
	
	self should: [ cartController checkout: cartID
							by: clientID
							usingCreditCard: (testObjectsFactory notExpiredCreditCard)
							debitWith: self
		]
		raise: Error
		withExceptionDo: [
			:anError | self assert: anError messageText equals: CartController cartSessionExpiredError.
		 ].! !


!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'testObjectsFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test01NewCartsAreCreatedEmpty

	self assert: testObjectsFactory createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [ cart add: testObjectsFactory itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 0 of: testObjectsFactory itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 2 of: testObjectsFactory itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test06CartRemembersAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore.
	self assert: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self deny: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: 2 of: testObjectsFactory itemSellByTheStore.
	self assert: (cart occurrencesOf: testObjectsFactory itemSellByTheStore) = 2! !


!CartTest methodsFor: 'setup' stamp: 'HernanWilkinson 6/17/2013 18:09'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.! !


!classDefinition: #CashierTest category: 'TusLibros'!
TestCase subclass: #CashierTest
	instanceVariableNames: 'testObjectsFactory debitBehavior'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:50'!
test01CanNotCheckoutAnEmptyCart

	| salesBook |
	
	salesBook := OrderedCollection new.
	self 
		should: [ Cashier 
			toCheckout: testObjectsFactory createCart 
			charging: testObjectsFactory notExpiredCreditCard 
			throught: self
			on: testObjectsFactory today
			registeringOn:  salesBook ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier cartCanNotBeEmptyErrorMessage.
			self assert: salesBook isEmpty ]! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:51'!
test02CalculatedTotalIsCorrect

	| cart cashier |
	
	cart := testObjectsFactory createCart.
	cart add: 2 of: testObjectsFactory itemSellByTheStore.
	
	cashier :=  Cashier
		toCheckout: cart 
		charging: testObjectsFactory notExpiredCreditCard 
		throught: self
		on: testObjectsFactory today 
		registeringOn: OrderedCollection new.
		
	self assert: cashier checkOut = (testObjectsFactory itemSellByTheStorePrice * 2)! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:51'!
test03CanNotCheckoutWithAnExpiredCreditCart

	| cart salesBook |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	salesBook := OrderedCollection new.
	
	self
		should: [ Cashier 
				toCheckout: cart 
				charging: testObjectsFactory expiredCreditCard 
				throught: self
				on: testObjectsFactory today
				registeringOn: salesBook ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError | 
			self assert: anError messageText = Cashier canNotChargeAnExpiredCreditCardErrorMessage.
			self assert: salesBook isEmpty ]! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 19:04'!
test04CheckoutRegistersASale

	| cart cashier salesBook total |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	salesBook := OrderedCollection new.
 
	cashier:= Cashier 
		toCheckout: cart 
		charging: testObjectsFactory notExpiredCreditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	total := cashier checkOut.
					
	self assert: salesBook size = 1.
	self assert: salesBook first total = total.! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 19:00'!
test05CashierChargesCreditCardUsingMerchantProcessor

	| cart cashier salesBook total creditCard debitedAmout debitedCreditCard  |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	creditCard := testObjectsFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
 
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	debitBehavior := [ :anAmount :aCreditCard | 
		debitedAmout := anAmount.
		debitedCreditCard := aCreditCard ].
	total := cashier checkOut.
					
	self assert: debitedCreditCard = creditCard.
	self assert: debitedAmout = total.! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:59'!
test06CashierDoesNotSaleWhenTheCreditCardHasNoCredit

	| cart cashier salesBook creditCard |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	creditCard := testObjectsFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
 	debitBehavior := [ :anAmount :aCreditCard | self error: Cashier creditCardHasNoCreditErrorMessage].
	
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	self 
		should: [cashier checkOut ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier creditCardHasNoCreditErrorMessage.
			self assert: salesBook isEmpty ]! !


!CashierTest methodsFor: 'setup' stamp: 'HernanWilkinson 6/17/2013 19:03'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.
	debitBehavior := [ :anAmount :aCreditCard | ]! !


!CashierTest methodsFor: 'merchant processor protocol' stamp: 'HernanWilkinson 6/17/2013 19:02'!
debit: anAmount from: aCreditCard 

	^debitBehavior value: anAmount value: aCreditCard ! !


!classDefinition: #AuthenticatorDouble category: 'TusLibros'!
Object subclass: #AuthenticatorDouble
	instanceVariableNames: 'actionExpected'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!AuthenticatorDouble methodsFor: 'authentication' stamp: 'pm 6/11/2022 13:10:22'!
authenticate: aClientID usingPassword: aPassword

	^actionExpected value.! !


!AuthenticatorDouble methodsFor: 'initialization' stamp: 'pm 6/11/2022 13:10:10'!
initializeExpectingAuthorized

	actionExpected := [true].! !

!AuthenticatorDouble methodsFor: 'initialization' stamp: 'pm 6/11/2022 13:10:06'!
initializeExpectingUnauthorized

	actionExpected := [false].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AuthenticatorDouble class' category: 'TusLibros'!
AuthenticatorDouble class
	instanceVariableNames: ''!

!AuthenticatorDouble class methodsFor: 'initilization' stamp: 'pm 6/9/2022 20:30:21'!
expectAuthorized

	^self new initializeExpectingAuthorized.! !

!AuthenticatorDouble class methodsFor: 'initilization' stamp: 'pm 6/9/2022 20:13:11'!
expectUnauthorized

	^self new initializeExpectingUnauthorized.! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:06'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 17:48'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'total' stamp: 'HernanWilkinson 6/17/2013 19:09'!
total

	^ items sum: [ :anItem | catalog at: anItem ]! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:51'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !


!Cart methodsFor: 'list' stamp: 'pm 6/9/2022 21:22:21'!
getItems

	^items.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #CartController category: 'TusLibros'!
Object subclass: #CartController
	instanceVariableNames: 'authenticator register salesBook transactionsByClient cartSessions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartController methodsFor: 'authentication' stamp: 'pm 6/11/2022 13:02:04'!
authenticate: aClientID usingPassword: aPassword

	| isAuthorized |
	
	isAuthorized := authenticator authenticate: aClientID usingPassword: aPassword.
	isAuthorized ifFalse: [^self error: CartController unauthorizedClientError]
	! !


!CartController methodsFor: 'create' stamp: 'om 6/12/2022 20:47:21'!
createCartFor: aClientID withPassword: aPassword

	| newCartID |
	
	self authenticate: aClientID usingPassword: aPassword.
	newCartID := self registerNewCart.	
	self registerCartSession: newCartID.
	
	^newCartID

	! !

!CartController methodsFor: 'create' stamp: 'om 6/12/2022 20:59:33'!
createCartFor: aClientID withPassword: aPassword at: aTimestamp

	| newCartID |
	
	self authenticate: aClientID usingPassword: aPassword.
	newCartID := self registerNewCart.	
	self registerCartSession: newCartID at: aTimestamp.
	
	^newCartID

	! !


!CartController methodsFor: 'register' stamp: 'om 6/12/2022 20:59:12'!
registerNewCart

	^register newCart.! !


!CartController methodsFor: 'initialization' stamp: 'om 6/12/2022 20:19:47'!
initializeWithAuthenticator: anAuthenticator catalog: aCatalog

	authenticator := anAuthenticator.
	register := CartRegister withCatalog: aCatalog.
	salesBook := OrderedCollection new.
	transactionsByClient := Dictionary new.
	cartSessions := Dictionary new.! !


!CartController methodsFor: 'list' stamp: 'om 6/12/2022 20:41:06'!
listCart: cartID

	| items |
	
	items := (register find: cartID) getItems.
	self assertCartSessionNotExpired: cartID.
	^items! !

!CartController methodsFor: 'list' stamp: 'pm 6/12/2022 19:25:17'!
listPurchasesFor: aClientID usingPassword: aPassword
	
	| clientPurchases |
	
	self authenticate: aClientID usingPassword: aPassword.
	
	clientPurchases := OrderedCollection new.
	transactionsByClient keysAndValuesDo: [ :transactionID :clientID | 
			clientID == aClientID ifTrue: (clientPurchases add: (salesBook at: transactionID))
	].

	^clientPurchases.! !


!CartController methodsFor: 'add' stamp: 'om 6/12/2022 20:42:59'!
addToCart: aCartID book: aBookISBN quantity: anAmount

	| cart |
	
	cart := register find: aCartID.
	self assertCartSessionNotExpired: aCartID.
	cart add: anAmount of: aBookISBN.! !


!CartController methodsFor: 'checkout' stamp: 'om 6/12/2022 21:25:12'!
checkout: aCartID by: aClientID usingCreditCard: aCreditCard debitWith: aMerchantProcessor

	| cashier cart |
	
	cart := register find: aCartID.
	self assertCartSessionNotExpired: aCartID.	
	
	cashier := Cashier toCheckout: cart
					charging: aCreditCard 
					throught: aMerchantProcessor 
					on: (Date today)
					registeringOn: salesBook.
	
	cashier checkOut.
	self deregisterCartSession: aCartID.
	
	^self registerTransactionToClient: aClientID.! !

!CartController methodsFor: 'checkout' stamp: 'pm 6/12/2022 19:33:12'!
registerTransactionToClient: aClientID

	| transactionID |
	
	transactionID := salesBook size.
	transactionsByClient at: transactionID put: aClientID.
	
	^transactionID ! !


!CartController methodsFor: 'sessions' stamp: 'om 6/12/2022 21:12:28'!
assertCartSessionNotExpired: aCartID

	(Time localMillisecondClock - (cartSessions at: aCartID)) > (30 * 60000)
		ifTrue: [^self error: CartController cartSessionExpiredError ].! !

!CartController methodsFor: 'sessions' stamp: 'om 6/12/2022 20:45:25'!
deregisterCartSession: aCartID

	cartSessions removeKey: aCartID.! !

!CartController methodsFor: 'sessions' stamp: 'om 6/12/2022 20:21:45'!
registerCartSession: aCartID.
		
		cartSessions at: aCartID put: (Time localMillisecondClock).
	! !

!CartController methodsFor: 'sessions' stamp: 'om 6/12/2022 21:00:00'!
registerCartSession: aCartID at: aTimestamp.
		
		cartSessions at: aCartID put: aTimestamp.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CartController class' category: 'TusLibros'!
CartController class
	instanceVariableNames: ''!

!CartController class methodsFor: 'initialization' stamp: 'pm 6/11/2022 17:22:07'!
withAccessManagedBy: anAuthenticator usingCatalog: aCatalog

	^self new initializeWithAuthenticator: anAuthenticator catalog: aCatalog.! !


!CartController class methodsFor: 'error' stamp: 'om 6/12/2022 21:00:33'!
cartSessionExpiredError

	^'Cart session expired'.! !

!CartController class methodsFor: 'error' stamp: 'pm 6/11/2022 12:55:19'!
unauthorizedClientError

	^'Unauthorized access'.! !


!classDefinition: #CartRegister category: 'TusLibros'!
Object subclass: #CartRegister
	instanceVariableNames: 'carts next_id catalog'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartRegister methodsFor: 'initialization' stamp: 'pm 6/9/2022 20:48:24'!
initialize

	carts := Dictionary new.
	next_id := 1.! !

!CartRegister methodsFor: 'initialization' stamp: 'pm 6/11/2022 14:16:05'!
initializeWithCatalog: aCatalog 
	
	catalog := aCatalog.! !


!CartRegister methodsFor: 'test' stamp: 'pm 6/9/2022 20:48:04'!
hasRegistered: aCartID

	^carts includesKey: aCartID.! !


!CartRegister methodsFor: 'register' stamp: 'pm 6/11/2022 14:16:55'!
newCart

	| newCartID |
	
	newCartID := next_id.
	carts at: next_id put: (Cart acceptingItemsOf: catalog).
	next_id := next_id + 1.
	
	^newCartID! !


!CartRegister methodsFor: 'find' stamp: 'pm 6/11/2022 14:47:22'!
find: aCartID

	(carts includesKey: aCartID) ifFalse: [self raiseCartNotFound].
	^carts at: aCartID.! !


!CartRegister methodsFor: 'error' stamp: 'pm 6/11/2022 14:49:51'!
raiseCartNotFound
	
	^self error: CartRegister cartNotFoundError.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CartRegister class' category: 'TusLibros'!
CartRegister class
	instanceVariableNames: ''!

!CartRegister class methodsFor: 'initialization' stamp: 'pm 6/11/2022 14:51:17'!
cartNotFoundError

	^'Cart not found'.! !

!CartRegister class methodsFor: 'initialization' stamp: 'pm 6/11/2022 14:15:47'!
withCatalog: aCatalog

	^self new initializeWithCatalog: aCatalog.! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'cart salesBook merchantProcessor creditCard total client'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:08'!
calculateTotal

	total := cart total.
	! !

!Cashier methodsFor: 'checkout - private' stamp: 'pm 6/12/2022 18:59:15'!
createSale

	^ Sale of: total withItems: (cart getItems).
! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:06'!
debitTotal

	merchantProcessor debit: total from: creditCard.
	! !

!Cashier methodsFor: 'checkout - private' stamp: 'pm 6/12/2022 18:01:05'!
registerSale

	salesBook add: self createSale.
	
! !


!Cashier methodsFor: 'checkout' stamp: 'pm 6/12/2022 18:00:42'!
checkOut

	self calculateTotal.
	self debitTotal.
	self registerSale.
	
	^total! !


!Cashier methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:53'!
initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor registeringOn: aSalesBook
	
	cart := aCart.
	creditCard := aCreditCard.
	merchantProcessor := aMerchantProcessor.
	salesBook := aSalesBook! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:22'!
assertIsNotEmpty: aCart 
	
	aCart isEmpty ifTrue: [self error: self cartCanNotBeEmptyErrorMessage ]! !

!Cashier class methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:23'!
assertIsNotExpired: aCreditCard on: aDate
	
	(aCreditCard isExpiredOn: aDate) ifTrue: [ self error: self canNotChargeAnExpiredCreditCardErrorMessage ]! !


!Cashier class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:51'!
toCheckout: aCart charging: aCreditCard throught: aMerchantProcessor on: aDate registeringOn: aSalesBook
	
	self assertIsNotEmpty: aCart.
	self assertIsNotExpired: aCreditCard on: aDate.
	
	^self new initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor registeringOn: aSalesBook! !


!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 18:21'!
canNotChargeAnExpiredCreditCardErrorMessage
	
	^'Can not charge an expired credit card'! !

!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:56'!
cartCanNotBeEmptyErrorMessage
	
	^'Can not check out an empty cart'! !

!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 19:02'!
creditCardHasNoCreditErrorMessage
	
	^'Credit card has no credit'! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'expiration'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 18:39'!
isExpiredOn: aDate 
	
	^expiration start < (Month month: aDate monthIndex year: aDate yearNumber) start ! !


!CreditCard methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:38'!
initializeExpiringOn: aMonth 
	
	expiration := aMonth ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:38'!
expiringOn: aMonth 
	
	^self new initializeExpiringOn: aMonth! !


!classDefinition: #Sale category: 'TusLibros'!
Object subclass: #Sale
	instanceVariableNames: 'total client items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Sale methodsFor: 'total' stamp: 'HernanWilkinson 6/17/2013 18:48'!
total
	
	^ total! !


!Sale methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:47'!
initializeTotal: aTotal

	total := aTotal ! !

!Sale methodsFor: 'initialization' stamp: 'pm 6/12/2022 18:58:03'!
initializeTotal: aTotal withItems: cartItems

	total := aTotal.
	items := cartItems.! !


!Sale methodsFor: 'test' stamp: 'pm 6/11/2022 17:13:56'!
correspondsTo: aClientID

	^client == aClientID! !


!Sale methodsFor: 'operations' stamp: 'pm 6/12/2022 19:00:49'!
= aSale

	^aSale total = total and: (aSale items = items). ! !


!Sale methodsFor: 'items' stamp: 'pm 6/12/2022 19:00:20'!
items

	^items.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Sale class' category: 'TusLibros'!
Sale class
	instanceVariableNames: ''!

!Sale class methodsFor: 'instance creation' stamp: 'pm 6/12/2022 18:57:35'!
of: aTotal withItems: cartItems

	"should assert total is not negative or 0!!"
	^self new initializeTotal: aTotal withItems: cartItems ! !


!classDefinition: #StoreTestObjectsFactory category: 'TusLibros'!
Object subclass: #StoreTestObjectsFactory
	instanceVariableNames: 'today'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemSellByTheStore
	
	^ 'validBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemSellByTheStorePrice
	
	^10! !


!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'HernanWilkinson 6/17/2013 18:08'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog! !

!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'HernanWilkinson 6/17/2013 18:08'!
defaultCatalog
	
	^ Dictionary new
		at: self itemSellByTheStore put: self itemSellByTheStorePrice;
		yourself ! !


!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'HernanWilkinson 6/17/2013 18:37'!
expiredCreditCard
	
	^CreditCard expiringOn: (Month month: today monthIndex year: today yearNumber - 1)! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'HernanWilkinson 6/17/2013 18:36'!
notExpiredCreditCard
	
	^CreditCard expiringOn: (Month month: today monthIndex year: today yearNumber + 1)! !


!StoreTestObjectsFactory methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:37'!
initialize

	today := DateAndTime now! !


!StoreTestObjectsFactory methodsFor: 'date' stamp: 'HernanWilkinson 6/17/2013 18:37'!
today
	
	^ today! !
