!classDefinition: #MarsRoverTest category: 'MarsRover'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:24:59'!
test01RoverStaysAtOriginWhenReceivesEmptyStringHeadingSouth
	|aDirection marsRover |
	aDirection := South new.
	marsRover := MarsRover heading: aDirection at: 0@0.
	
	self assert: ((marsRover process: '') isAt: 0@0 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:25:03'!
test02RoverStaysAtOriginWhenReceivesEmptyStringHeadingNorth
	| aDirection marsRover |
	aDirection := North new.
	marsRover := MarsRover heading: aDirection at: 0@0.
	
	self assert: ((marsRover process: '') isAt: 0@0 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:25:07'!
test03RoverStaysAtOriginFacingEastWhenReceivesRStringHeadingNorth
	| marsRover |
	
	marsRover := MarsRover heading: North new at: 0@0.
	
	self assert: ((marsRover process: 'R') isAt: 0@0 heading: East new)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:25:11'!
test04RoverStaysAtOriginFacingWestWhenReceivesLStringHeadingNorth
	| marsRover |
	
	marsRover := MarsRover heading: North new at: 0@0.
	
	self assert: ((marsRover process: 'L') isAt: 0@0 heading: West new)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:25:15'!
test05RoverStaysAtOriginFacingSouthWhenReceivesRStringHeadingWest
	| marsRover |
	
	marsRover := MarsRover heading: West new at: 0@0.
	
	self assert: ((marsRover process: 'R') isAt: 0@0 heading: South new)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:25:23'!
test06RoverStaysAtOriginFacingWestWhenReceivesRStringHeadingSouth
	| marsRover |
	
	marsRover := MarsRover heading: South new at: 0@0.
	
	self assert: ((marsRover process: 'R') isAt: 0@0 heading: West new)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:25:27'!
test07RoverStaysAtOriginFacingSouthWhenReceivesRStringHeadingEast
	| marsRover |
	
	marsRover := MarsRover heading: East new at: 0@0.
	
	self assert: ((marsRover process: 'R') isAt: 0@0 heading: South new)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:25:31'!
test08RoverStaysAtOriginFacingSouthWhenReceivesLStringHeadingEast
	| marsRover |
	
	marsRover := MarsRover heading: East new at: 0@0.
	
	self assert: ((marsRover process: 'L') isAt: 0@0 heading: South new)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:25:36'!
test09RoverStaysAtOriginFacingEastWhenReceivesLStringHeadingSouth
	| marsRover |
	
	marsRover := MarsRover heading: South new at: 0@0.
	
	self assert: ((marsRover process: 'L') isAt: 0@0 heading: East new)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:25:40'!
test10RoverStaysAtOriginFacingSouthWhenReceivesLStringHeadingWest
	| marsRover |
	
	marsRover := MarsRover heading: West new at: 0@0.
	
	self assert: ((marsRover process: 'L') isAt: 0@0 heading: South new)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 21:50:36'!
test11RoverStaysOnOriginSpecifiedWhenReceivesEmptyString
	
	|aDirection marsRover |
	
	aDirection:= South new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	
	self assert: ((marsRover process: '') isAt: 0@1 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 21:52:42'!
test12RoverMovesAStepTowardsSouthWhenIndicatedForwardHeadingSouth
	|aDirection marsRover |
	aDirection:= South new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	
	self assert: ((marsRover process: 'F') isAt: 0@0 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 21:53:27'!
test13RoverMovesMultipleStepsTowardsSouthWhenIndicatedForwardHeadingSouth
	|aDirection marsRover |
	aDirection:= South new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	marsRover := marsRover process: 'F'.
	self assert: ((marsRover process: 'F') isAt: 0@-1 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 21:53:55'!
test14RoverMovesAStepTowardsNorthWhenIndicatedForwardHeadingNorth
	|aDirection marsRover |
	aDirection:= North new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	self assert: ((marsRover process: 'F') isAt: 0@2 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 21:55:01'!
test15RoverMovesMultipleStepsTowardsNorthWhenIndicatedForwardHeadingNorth
	|aDirection marsRover |
	aDirection:= North new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	marsRover:= marsRover process: 'F'.
	self assert: ((marsRover process: 'F') isAt: 0@3 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 21:55:42'!
test16RoverMovesAStepTowardsEastWhenIndicatedForwardHeadingEast
	|aDirection marsRover |
	aDirection:= East new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	self assert: ((marsRover process: 'F') isAt: 1@1 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 21:56:07'!
test17RoverMovesMultipleStepsTowardsEastWhenIndicatedForwardHeadingEast
	|aDirection marsRover |
	aDirection:= East new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	marsRover:= marsRover process:'F'.
	self assert: ((marsRover process: 'F') isAt: 2@1 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:03:11'!
test18RoverMovesAStepTowardsWestWhenIndicatedForwardHeadingWest
	|aDirection marsRover |
	aDirection:= West new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	self assert: ((marsRover process: 'F') isAt: -1@1 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:02:52'!
test19RoverMovesMultipleStepsTowardsWestWhenIndicatedForwardHeadingWest
	|aDirection marsRover |
	aDirection:= West new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	marsRover:= marsRover process:'F'.
	self assert: ((marsRover process: 'F') isAt: -2@1 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:03:46'!
test20RoverMovesAStepTowardsEastWhenIndicatedBackwardHeadingWest
	|aDirection marsRover |
	aDirection:= West new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	self assert: ((marsRover process: 'B') isAt: 1@1 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:05:42'!
test21RoverMovesAStepTowardsNorthWhenIndicatedBackwardHeadingSouth
	|aDirection marsRover |
	aDirection:= South new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	self assert: ((marsRover process: 'B') isAt: 0@2 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:09:40'!
test22RoverMovesMultipleStepsTowardsNorthWhenIndicatedBackwardHeadingSouth
	|aDirection marsRover |
	aDirection:= South new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	marsRover process: 'B'.
	self assert: ((marsRover process: 'B') isAt: 0@3 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:14:56'!
test23RoverMovesMultipleStepsTowardsEastWhenIndicatedBackwardHeadingWest
	|aDirection marsRover |
	aDirection:= West new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	marsRover process: 'B'.
	self assert: ((marsRover process: 'B') isAt: 2@1 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:11:43'!
test24RoverMovesAStepTowardsWestWhenIndicatedBackwardHeadingEast
	|aDirection marsRover |
	aDirection:= East new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	self assert: ((marsRover process: 'B') isAt: -1@1 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:12:23'!
test25RoverMovesMultipleStepsTowardsWestWhenIndicatedBackwardHeadingEast
	|aDirection marsRover |
	aDirection:= East new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	marsRover process: 'B'.
	self assert: ((marsRover process: 'B') isAt: -2@1 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:13:09'!
test26RoverMovesAStepTowardsSouthWhenIndicatedBackwardHeadingNorth
	|aDirection marsRover |
	aDirection:= North new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	self assert: ((marsRover process: 'B') isAt: 0@0 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:14:06'!
test27RoverMovesMultipleStepsTowardsSouthWhenIndicatedBackwardHeadingNorth
	|aDirection marsRover |
	aDirection:= North new.
	marsRover := MarsRover heading: aDirection at: 0@1.
	marsRover process: 'B'.
	self assert: ((marsRover process: 'B') isAt: 0@-1 heading: aDirection)! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:32:12'!
test28RoverStopsFunctioningWhenReceivesInvalidMessage
	| marsRover aDirection |
	
	aDirection := North new.
	
	marsRover := MarsRover heading: aDirection at: 0@0.
	marsRover process: 'NOP'.
	self assert: ((marsRover process: 'R') isAt: 0@0 heading: aDirection)! !


!classDefinition: #Cardinal category: 'MarsRover'!
Object subclass: #Cardinal
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!Cardinal methodsFor: 'move' stamp: 'pm 5/15/2022 21:41:11'!
moveBackward: aRover

	self subclassResponsibility.! !

!Cardinal methodsFor: 'move' stamp: 'pm 5/15/2022 21:39:18'!
moveForward: aRover

	self subclassResponsibility.! !


!Cardinal methodsFor: 'rotate' stamp: 'pm 5/12/2022 21:47:39'!
rotateLeft

	self subclassResponsibility.! !

!Cardinal methodsFor: 'rotate' stamp: 'pm 5/12/2022 21:47:30'!
rotateRight

	self subclassResponsibility.! !


!Cardinal methodsFor: 'operators' stamp: 'mk 5/14/2022 18:25:00'!
= aCardinal
	^self className = aCardinal className.! !


!classDefinition: #East category: 'MarsRover'!
Cardinal subclass: #East
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!East methodsFor: 'rotate' stamp: 'pm 5/15/2022 22:45:23'!
whenRotatingLeft: aRover
	^aRover rotateToSouth.! !

!East methodsFor: 'rotate' stamp: 'pm 5/15/2022 22:45:27'!
whenRotatingRight: aRover
	^aRover rotateToSouth.! !


!East methodsFor: 'move' stamp: 'pm 5/15/2022 22:44:57'!
moveBackward: aRover
	^aRover moveToWest.! !

!East methodsFor: 'move' stamp: 'pm 5/15/2022 22:46:47'!
moveForward: aRover
	^aRover moveToEast.! !


!classDefinition: #North category: 'MarsRover'!
Cardinal subclass: #North
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!North methodsFor: 'rotate' stamp: 'pm 5/15/2022 22:45:34'!
whenRotatingLeft: aRover
	^aRover rotateToWest.! !

!North methodsFor: 'rotate' stamp: 'pm 5/15/2022 22:45:38'!
whenRotatingRight: aRover
	^aRover rotateToEast.! !


!North methodsFor: 'move' stamp: 'pm 5/15/2022 22:46:34'!
moveBackward: aRover
	^aRover moveToSouth.! !

!North methodsFor: 'move' stamp: 'pm 5/15/2022 22:46:30'!
moveForward: aRover
	^aRover moveToNorth.! !


!classDefinition: #South category: 'MarsRover'!
Cardinal subclass: #South
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!South methodsFor: 'rotate' stamp: 'pm 5/15/2022 22:45:48'!
whenRotatingLeft: aRover
	^aRover rotateToEast.! !

!South methodsFor: 'rotate' stamp: 'pm 5/15/2022 22:45:43'!
whenRotatingRight: aRover
	^aRover rotateToWest.
	! !


!South methodsFor: 'move' stamp: 'pm 5/15/2022 22:46:23'!
moveBackward: aRover
	^aRover moveToNorth.! !

!South methodsFor: 'move' stamp: 'pm 5/15/2022 22:46:19'!
moveForward: aRover 
	^aRover moveToSouth.! !


!classDefinition: #West category: 'MarsRover'!
Cardinal subclass: #West
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!West methodsFor: 'rotate' stamp: 'pm 5/15/2022 22:45:57'!
whenRotatingLeft: aRover
	^aRover rotateToSouth.
	! !

!West methodsFor: 'rotate' stamp: 'pm 5/15/2022 22:45:54'!
whenRotatingRight: aRover
	^aRover rotateToSouth.
	! !


!West methodsFor: 'move' stamp: 'pm 5/15/2022 22:46:09'!
moveBackward: aRover
	^aRover moveToEast.! !

!West methodsFor: 'move' stamp: 'pm 5/15/2022 22:46:06'!
moveForward: aRover
	^aRover moveToWest.! !


!classDefinition: #MarsRover category: 'MarsRover'!
Object subclass: #MarsRover
	instanceVariableNames: 'direction position status'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRover methodsFor: 'initializate' stamp: 'pm 5/15/2022 22:54:43'!
initializeHeading: aDirection at: aPosition

	direction := aDirection.
	position := aPosition.
	status := RoverOnline for: self.! !


!MarsRover methodsFor: 'move' stamp: 'pm 5/15/2022 22:22:19'!
moveToEast
	
	| newX |
	
	newX := position x + 1.
	position := newX@position y ! !

!MarsRover methodsFor: 'move' stamp: 'pm 5/15/2022 22:22:55'!
moveToNorth 
	
	| newY |
	
	newY := position y + 1.
	position := position x@newY! !

!MarsRover methodsFor: 'move' stamp: 'pm 5/15/2022 22:23:24'!
moveToSouth 
	
	| newY |
	
	newY := position y - 1.
	position := position x@newY ! !

!MarsRover methodsFor: 'move' stamp: 'pm 5/15/2022 22:23:50'!
moveToWest
	
	| newX |
	
	newX := position x - 1.
	position := newX@position y ! !


!MarsRover methodsFor: 'rotate' stamp: 'mk 5/14/2022 17:50:08'!
rotateToEast
	direction:= East new.! !

!MarsRover methodsFor: 'rotate' stamp: 'mk 5/14/2022 17:51:09'!
rotateToNorth
	direction:= North new.! !

!MarsRover methodsFor: 'rotate' stamp: 'mk 5/14/2022 17:50:33'!
rotateToSouth
	direction:= South new.! !

!MarsRover methodsFor: 'rotate' stamp: 'mk 5/14/2022 17:50:48'!
rotateToWest
	direction:= West new.! !


!MarsRover methodsFor: 'test' stamp: 'mk 5/14/2022 19:05:45'!
isAt: aPosition heading: aDirection

	^aPosition = position and: aDirection = direction! !


!MarsRover methodsFor: 'process' stamp: 'pm 5/15/2022 22:58:03'!
process: aCommand
		
	status receiveCommand: aCommand.! !

!MarsRover methodsFor: 'process' stamp: 'pm 5/15/2022 22:57:33'!
processWhileOnline: aCommand
		
	aCommand = 'R' ifTrue: [ ^direction whenRotatingRight: self ].
	aCommand = 'L' ifTrue: [ ^direction whenRotatingLeft: self ].
	aCommand = 'F' ifTrue: [ ^direction moveForward: self ].
	aCommand = 'B' ifTrue: [ ^direction moveBackward: self		].
	aCommand = '' ifFalse: [ status := RoverOffline for: self ].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover'!
MarsRover class
	instanceVariableNames: ''!

!MarsRover class methodsFor: 'as yet unclassified' stamp: 'mk 5/14/2022 19:03:05'!
heading: aDirection at: aPosition
	^self new initializeHeading: aDirection at: aPosition.! !


!classDefinition: #RoverStatus category: 'MarsRover'!
Object subclass: #RoverStatus
	instanceVariableNames: 'direction position status rover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!RoverStatus methodsFor: 'initialize' stamp: 'pm 5/15/2022 22:51:19'!
initializeFor: aRover

	rover := aRover.! !


!RoverStatus methodsFor: 'process' stamp: 'pm 5/15/2022 22:52:55'!
receiveCommand: aCommand

	self subclassResponsibility.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RoverStatus class' category: 'MarsRover'!
RoverStatus class
	instanceVariableNames: ''!

!RoverStatus class methodsFor: 'as yet unclassified' stamp: 'pm 5/15/2022 22:51:02'!
for: aRover

	^self new initializeFor: aRover	! !


!classDefinition: #RoverOffline category: 'MarsRover'!
RoverStatus subclass: #RoverOffline
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!RoverOffline methodsFor: 'process' stamp: 'pm 5/15/2022 22:58:36'!
receiveCommand: aCommand! !


!classDefinition: #RoverOnline category: 'MarsRover'!
RoverStatus subclass: #RoverOnline
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!RoverOnline methodsFor: 'process' stamp: 'pm 5/15/2022 22:55:15'!
receiveCommand: aCommand

	rover processWhileOnline: aCommand.! !
