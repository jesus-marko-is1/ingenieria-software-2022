!classDefinition: #AccountSummaryTest category: 'Portfolio-Solucion'!
TestCase subclass: #AccountSummaryTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!AccountSummaryTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:16:13'!
test01NewSummaryReportsNothing

	| summary |
	
	summary := AccountSummary new.
	
	self assert: (summary present) equals: (OrderedCollection new).! !

!AccountSummaryTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:17:04'!
test02WithAccountReportOnlyBalanceOfCero

	| summary account log |
	
	account := ReceptiveAccount new.
	
	summary := AccountSummary for: account.
	
	log := OrderedCollection with: 'Balance = 0'.
	
	self assert: (summary present) equals: log.! !

!AccountSummaryTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:18:02'!
test03ReportsDepositAndBalaceAccordingly

	| summary account log |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	
	summary := AccountSummary for: account.
	
	log := OrderedCollection with: 'Dep�sito por 100.' with: 'Balance = 100'.
	
	self assert: (summary present) equals: log.! !

!AccountSummaryTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:18:46'!
test04ReportsWithdrawAndBalanceAccordingly

	| summary account log |
	
	account := ReceptiveAccount new.
	Withdraw register: 100 on: account.
	
	summary := AccountSummary for: account.
	
	log := OrderedCollection with: 'Extracci�n por 100.' with: 'Balance = -100'.
	
	self assert: (summary present) equals: log.! !

!AccountSummaryTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:19:16'!
test05ReportsTransferExtractionAndBalanceAccordingly

	| summary account log |
	
	account := ReceptiveAccount new.
	
	Transfer from: account to: (ReceptiveAccount new) amount: 100.
	
	summary := AccountSummary for: account.
	
	log := OrderedCollection with: 'Salida por transferencia de 100.' with: 'Balance = -100'.
	
	self assert: (summary present) equals: log.! !

!AccountSummaryTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:19:35'!
test06ReportsTransferDepositAndBalanceAccordingly

	| summary account log |
	
	account := ReceptiveAccount new.
	
	Transfer from: (ReceptiveAccount new) to: account  amount: 100.
	
	summary := AccountSummary for: account.
	
	log := OrderedCollection with: 'Entrada por transferencia de 100.' with: 'Balance = 100'.
	
	self assert: (summary present) equals: log.! !

!AccountSummaryTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:20:15'!
test07WithPortfolioReportOnlyBalanceOfCero

	| summary portfolio log |
	
	portfolio := Portfolio new.
	
	summary := AccountSummary for: portfolio.
	
	log := OrderedCollection with: 'Balance = 0'.
	
	self assert: (summary present) equals: log.! !

!AccountSummaryTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:20:49'!
test08WithPortfolioWithAccountReportsAccordingly

	| summary account log portfolio |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	
	portfolio := Portfolio new add: account.
	
	summary := AccountSummary for: portfolio.
	
	
	log := OrderedCollection with: 'Dep�sito por 100.' with: 'Balance = 100'.
	
	self assert: (summary present) equals: log.! !

!AccountSummaryTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:21:22'!
test09WithPortfolioWithManyAccountsReportsAccordingly

	| summary account log portfolio anotherAccount |
	
	account := ReceptiveAccount new.
	anotherAccount := ReceptiveAccount new.
	
	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
	
	portfolio := (Portfolio new add: account) add: anotherAccount.
	
	summary := AccountSummary for: portfolio.
	
	log := OrderedCollection with: 'Dep�sito por 100.' with: 'Extracci�n por 50.' with: 'Balance = 50'.
	
	self assert: (summary present) equals: log.! !


!classDefinition: #PortfolioTest category: 'Portfolio-Solucion'!
TestCase subclass: #PortfolioTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:16:26'!
test01BalanceOfPortfolioWithoutAccountsIsZero

	self assert: 0 equals: Portfolio new balance! !

!PortfolioTest methodsFor: 'tests' stamp: 'NR 5/27/2021 17:36:04'!
test02BalanceOfPortfolioWithAccountsIsSumOfAccountsBalance

	| account portfolio |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	portfolio := Portfolio with: account.
	
	self assert: account balance equals: portfolio balance! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:23:25'!
test03BalanceOfPortfolioIsCalculatedRecursivelyOnPortfolios

	| simplePortfolioAccount simplePortfolio composedPortfolioAccount composedPortofolio |
	
	simplePortfolioAccount := ReceptiveAccount new.
	Deposit register: 100 on: simplePortfolioAccount.
	simplePortfolio := Portfolio with: simplePortfolioAccount.
	
	composedPortfolioAccount := ReceptiveAccount new.
	Withdraw register: 50 on: composedPortfolioAccount.
	composedPortofolio := Portfolio with: simplePortfolio with: composedPortfolioAccount.
	
	self assert: (composedPortfolioAccount balance + simplePortfolio balance) equals: composedPortofolio balance! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:43:15'!
test04PortfolioWithoutAccountsHasNoRegisteredTransaction

	self deny: (Portfolio new hasRegistered: (Deposit for: 100))! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:43:11'!
test05PortfolioHasRegisteredItsAccountsTransactions

	| account portfolio deposit |
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	portfolio := Portfolio with: account.
	
	self assert: (portfolio hasRegistered: deposit)! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:58:06'!
test06PortfolioLooksForRegisteredTransactionsRecursively

	| simplePortfolioAccount simplePortfolio composedPortfolioAccount composedPortfolio composedPortfolioAccountWithdraw simplePortfolioAccountDeposit |
	
	simplePortfolioAccount := ReceptiveAccount new.
	simplePortfolioAccountDeposit := Deposit register: 100 on: simplePortfolioAccount.
	simplePortfolio := Portfolio with: simplePortfolioAccount.
	
	composedPortfolioAccount := ReceptiveAccount new.
	composedPortfolioAccountWithdraw := Withdraw register: 50 on: composedPortfolioAccount.
	composedPortfolio := Portfolio with: simplePortfolio with: composedPortfolioAccount.
	
	self assert: (composedPortfolio hasRegistered: simplePortfolioAccountDeposit).
	self assert: (composedPortfolio hasRegistered: composedPortfolioAccountWithdraw)! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:58:10'!
test07PortfolioHasNoTransactionWhenHasNoAccounts

	self assert: Portfolio new transactions isEmpty! !

!PortfolioTest methodsFor: 'tests' stamp: 'NR 6/22/2020 07:31:19'!
test08PortfolioTransactionsIncludesAllItsAccountsTransactions

	| account portfolio accountDeposit anotherAccount portfolioTransactions anotherAccountWithdraw |
	
	account := ReceptiveAccount new.
	accountDeposit := Deposit register: 100 on: account.
	anotherAccount := ReceptiveAccount new.
	anotherAccountWithdraw := Withdraw register: 100 on: account.
	portfolio := Portfolio with: account.
	
	portfolioTransactions := portfolio transactions.
	
	self assert: 2 equals: portfolioTransactions size.
	self assert: (portfolioTransactions includes: accountDeposit).
	self assert: (portfolioTransactions includes: anotherAccountWithdraw)! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:58:20'!
test09PortfolioTransactionsAreCalculatedRecursively

	| simplePortfolioAccount simplePortfolio composedPortfolioAccount composedPortfolio composedPortfolioAccountWithdraw simplePortfolioAccountDeposit composedPortfolioTransactions |
	
	simplePortfolioAccount := ReceptiveAccount new.
	simplePortfolioAccountDeposit := Deposit register: 100 on: simplePortfolioAccount.
	simplePortfolio := Portfolio with: simplePortfolioAccount.
	
	composedPortfolioAccount := ReceptiveAccount new.
	composedPortfolioAccountWithdraw := Withdraw register: 50 on: composedPortfolioAccount.
	composedPortfolio := Portfolio with: simplePortfolio with: composedPortfolioAccount.
	
	composedPortfolioTransactions := composedPortfolio transactions.
	self assert: 2 equals: composedPortfolioTransactions size.
	self assert: (composedPortfolioTransactions includes: simplePortfolioAccountDeposit).
	self assert: (composedPortfolioTransactions includes: composedPortfolioAccountWithdraw)! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:58:24'!
test10PortfolioCanNotIncludeTheSameAccountMoreThanOnce

	| account portfolio |
	
	account := ReceptiveAccount new.
	portfolio := Portfolio with: account.
	
	self 
		should: [ portfolio add: account ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: Portfolio canNotAddAccountErrorMessage equals: anError messageText.
			self assert: 1 equals: portfolio accountsSize.
			self assert: (portfolio accountsIncludes: account) ]! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:58:28'!
test11PortfolioCanNotIncludeAccountOfItsPortfolios

	| account simplePortfolio composedPortfolio |
	
	account := ReceptiveAccount new.
	simplePortfolio := Portfolio with: account.
	composedPortfolio := Portfolio with: simplePortfolio.
	
	self 
		should: [ composedPortfolio add: account ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: Portfolio canNotAddAccountErrorMessage equals: anError messageText.
			self assert: 1 equals: composedPortfolio accountsSize.
			self assert: (composedPortfolio accountsIncludes: simplePortfolio) ]! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:58:32'!
test12PortfolioCanNotIncludeItself

	| account simplePortfolio |
	
	account := ReceptiveAccount new.
	simplePortfolio := Portfolio with: account.
	
	self 
		should: [ simplePortfolio add: simplePortfolio ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: Portfolio canNotAddAccountErrorMessage equals: anError messageText.
			self assert: 1 equals: simplePortfolio accountsSize.
			self assert: (simplePortfolio accountsIncludes: account) ]! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 12:01:51'!
test13ComposedPortfolioCanNotHaveParentPortfolioAccount

	| account simplePortfolio composedPortfolio |
	
	account := ReceptiveAccount new.
	simplePortfolio := Portfolio new.
	composedPortfolio := Portfolio with: simplePortfolio.
	composedPortfolio add: account.
	
	self 
		should: [ simplePortfolio add: account ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: Portfolio canNotAddAccountErrorMessage equals: anError messageText.
			self assert: simplePortfolio accountsIsEmpty ]! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 12:12:16'!
test14ComposedPortfolioCanNotHaveAccountOfAnyRootParentRecursively

	| account leftParentPortfolio leftRootParentPortfolio portfolio rightParentPortfolio rightRootParentPortfolio |
	
	account := ReceptiveAccount new.
	portfolio := Portfolio new.
	leftParentPortfolio := Portfolio with: portfolio .
	leftRootParentPortfolio := Portfolio with: leftParentPortfolio.
	leftRootParentPortfolio add: account.
	
	rightParentPortfolio := Portfolio with: portfolio .
	rightRootParentPortfolio := Portfolio with: rightParentPortfolio.
	rightRootParentPortfolio add: account.

	self 
		should: [ portfolio add: account ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: Portfolio canNotAddAccountErrorMessage equals: anError messageText.
			self assert: portfolio accountsIsEmpty ]! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/29/2019 16:31:18'!
test15PortfolioCanNotIncludeAnyOfTheComposedAccountOfPortfolioToAdd

	| portfolioToAdd portfolioToModify rootPortfolio sharedAccount |
	
	sharedAccount := ReceptiveAccount new.
	portfolioToModify := Portfolio new.
	rootPortfolio := Portfolio with: sharedAccount with: portfolioToModify.
	portfolioToAdd := Portfolio with: sharedAccount.
	
	self 
		should: [ portfolioToModify add: portfolioToAdd ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError | 
			self assert: Portfolio canNotAddAccountErrorMessage equals: anError messageText.
			self assert: portfolioToModify accountsIsEmpty ]! !


!classDefinition: #ReceptiveAccountTest category: 'Portfolio-Solucion'!
TestCase subclass: #ReceptiveAccountTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:19:48'!
test01ReceptiveAccountHaveZeroAsBalanceWhenCreated 

	| account |
	
	account := ReceptiveAccount new.

	self assert: 0 equals: account balance .
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:19:54'!
test02DepositIncreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
		
	self assert: 100 equals: account balance .
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:02'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
		
	self assert: 50 equals: account balance .
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:32'!
test04WithdrawValueMustBePositive 

	| account withdrawValue |
	
	account := ReceptiveAccount new.
	withdrawValue := 50.
	
	self assert: withdrawValue equals: (Withdraw register: withdrawValue on: account) value
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:46'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	withdraw := Withdraw register: 50 on: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:54'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit :=  Deposit for: 100.
	withdraw := Withdraw for: 50.
		
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered:withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:21:24'!
test07AccountKnowsItsTransactions 

	| account1 deposit1 |
	
	account1 := ReceptiveAccount new.
	
	deposit1 := Deposit register: 100 on: account1.
		
	self assert: 1 equals: account1 transactions size .
	self assert: (account1 transactions includes: deposit1).
! !


!classDefinition: #TransferNetTest category: 'Portfolio-Solucion'!
TestCase subclass: #TransferNetTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!TransferNetTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:05:46'!
test01NewTransferNetReportsNothing

	| neto |
	
	neto := TrasferNet new.
	
	self assert: (neto present) equals: (String new).! !

!TransferNetTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:06:15'!
test02WithEmptyAccountReports0

	| neto account|
	account := ReceptiveAccount new.
	neto := TrasferNet for: account.
	
	self assert: (neto present) equals: ('El neto de transferencias es 0').! !

!TransferNetTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:07:30'!
test03WithAccountWithTransactionReportsTransactionValue

	| neto account amount|
	amount := 100.
	account := ReceptiveAccount new.
	Transfer from: account to: (ReceptiveAccount new) amount: amount.
	neto := TrasferNet for: account.
	
	self assert: (neto present) equals: ('El neto de transferencias es ', (amount * -1 ) asString).! !

!TransferNetTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:08:54'!
test04DoesNotTakeIntoAccountDeposits

	| neto account amount|
	amount := 100.
	account := ReceptiveAccount new.
	Transfer from: account to: (ReceptiveAccount new) amount: amount.
	Deposit register: amount on: account.
	neto := TrasferNet for: account.
	
	self assert: (neto present) equals: ('El neto de transferencias es ', (amount * -1 ) asString).! !

!TransferNetTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:09:14'!
test05DoesNotTakeIntoAccountWithdraws

	| neto account amount|
	amount := 100.
	account := ReceptiveAccount new.
	Transfer from: (ReceptiveAccount new) to: account amount: amount.
	Withdraw register: amount on: account.
	neto := TrasferNet for: account.
	
	self assert: (neto present) equals: ('El neto de transferencias es ', amount asString).! !


!classDefinition: #TransferTest category: 'Portfolio-Solucion'!
TestCase subclass: #TransferTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!TransferTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:11:26'!
test01TransferImpactAccountsBalanceAsExpected

	| accountFrom accountTo amount |
	accountFrom := ReceptiveAccount new.
	accountTo := ReceptiveAccount new.
	
	amount := 50.
	
	Transfer from: accountFrom to: accountTo amount: amount.
	
	self assert: (accountFrom balance) equals: amount * (-1).
	self assert: (accountTo balance) equals: amount.! !

!TransferTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:11:59'!
test02TransferGetsRegisteredInBothAccounts

	| accountFrom accountTo amount transference |
	accountFrom := ReceptiveAccount new.
	accountTo := ReceptiveAccount new.
	
	amount := 50.
	
	transference := Transfer from: accountFrom to: accountTo amount: amount.
	
	self assert: (accountFrom hasRegistered: transference extraction) equals: true.
	self assert: (accountTo hasRegistered: transference deposit) equals: true.! !

!TransferTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:12:24'!
test03OnlyOneObjectPerTransactionIsRegistered

	| accountFrom accountTo amount |
	accountFrom := ReceptiveAccount new.
	accountTo := ReceptiveAccount new.
	
	amount := 50.
	
	Transfer from: accountFrom to: accountTo amount: amount.
	
	self assert: (accountFrom transactions size) equals: 1.
	self assert: (accountTo transactions size) equals: 1.! !

!TransferTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:13:13'!
test04TransferCanBeAccessedByItsParts

	| accountFrom accountTo amount transference transactionDeposit transactionExtraction |

	accountFrom := ReceptiveAccount new.
	accountTo := ReceptiveAccount new.
	
	amount := 50.
	
	transference := Transfer from: accountFrom to: accountTo amount: amount.
	
	transactionExtraction := accountFrom transactions last.
	transactionDeposit := accountTo transactions last.
	
	self assert: transactionDeposit transference equals: transference.
	self assert: transactionExtraction transference equals: transference.! !

!TransferTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:13:39'!
test05EachPartCanAccessTheOther

	| accountFrom accountTo amount transactionDeposit transactionExtraction |

	accountFrom := ReceptiveAccount new.
	accountTo := ReceptiveAccount new.
	
	amount := 50.
	
	Transfer from: accountFrom to: accountTo amount: amount.
	
	transactionExtraction := accountFrom transactions last.
	transactionDeposit := accountTo transactions last.
	
	self assert: (transactionDeposit counterpart) equals: transactionExtraction.
	self assert: (transactionExtraction counterpart) equals: transactionDeposit.! !

!TransferTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:14:37'!
test06AssertCannotSendMoneyToOneself

	| account |

	account := ReceptiveAccount new.
	 
	self 
		should: [Transfer from: account to: account amount: 100]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText 
									equals: 'No es posible enviar dinero a uno mismo.'].! !

!TransferTest methodsFor: 'as yet unclassified' stamp: 'pm 5/29/2022 20:15:01'!
test07AssertCannotTransferLessOrEqualToCero

	| accountSender accountReceiver |

	accountSender := ReceptiveAccount new.
	accountReceiver := ReceptiveAccount new.
		
	self 
		should: [Transfer from: accountSender to: accountReceiver amount: 0]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText 
									equals: 'La cantidad enviada debe ser mayor a 0.'].! !


!classDefinition: #Account category: 'Portfolio-Solucion'!
Object subclass: #Account
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!Account methodsFor: 'testing' stamp: 'HAW 5/25/2019 12:23:47'!
hasRegistered: aTransaction

	self subclassResponsibility ! !

!Account methodsFor: 'testing' stamp: 'HAW 5/25/2019 12:24:25'!
isComposedBy: anAccount

	self subclassResponsibility ! !


!Account methodsFor: 'balance' stamp: 'HAW 5/25/2019 12:23:40'!
balance

	self subclassResponsibility ! !


!Account methodsFor: 'transactions' stamp: 'HAW 5/25/2019 12:23:27'!
addTransactionsTo: aCollectionOfTransactions

	self subclassResponsibility ! !

!Account methodsFor: 'transactions' stamp: 'HAW 5/25/2019 12:23:15'!
transactions

	self subclassResponsibility ! !


!Account methodsFor: 'composition' stamp: 'HAW 5/25/2019 12:24:04'!
addedTo: aPortfolio

	self subclassResponsibility ! !


!classDefinition: #Portfolio category: 'Portfolio-Solucion'!
Account subclass: #Portfolio
	instanceVariableNames: 'accounts parents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!Portfolio methodsFor: 'accounts management' stamp: 'HAW 5/25/2019 11:49:20'!
accountsIncludes: anAccount

	^accounts includes: anAccount ! !

!Portfolio methodsFor: 'accounts management' stamp: 'HAW 5/25/2019 12:05:04'!
accountsIsEmpty
	
	^accounts isEmpty ! !

!Portfolio methodsFor: 'accounts management' stamp: 'HAW 5/25/2019 11:49:06'!
accountsSize
	
	^accounts size! !

!Portfolio methodsFor: 'accounts management' stamp: 'HAW 5/25/2019 12:19:20'!
add: accountToAdd

	self assertCanAdd: accountToAdd.
		
	accounts add: accountToAdd.
	accountToAdd addedTo: self 
	! !

!Portfolio methodsFor: 'accounts management' stamp: 'HAW 5/25/2019 12:17:31'!
rootParents
	
	| rootParents |
	
	rootParents := Set new.
	self addRootParentsTo: rootParents.
	
	^ rootParents! !


!Portfolio methodsFor: 'initialization' stamp: 'HAW 5/25/2019 12:03:18'!
initialize

	accounts := OrderedCollection new.
	parents := OrderedCollection new.! !


!Portfolio methodsFor: 'balance' stamp: 'HAW 5/25/2019 11:19:36'!
balance
	
	^accounts sum: [ :anAccount | anAccount balance ] ifEmpty: [ 0 ]! !


!Portfolio methodsFor: 'transactions' stamp: 'HAW 5/25/2019 11:42:55'!
addTransactionsTo: aCollectionOfTransactions

	accounts do: [ :anAccount | anAccount addTransactionsTo: aCollectionOfTransactions ]! !

!Portfolio methodsFor: 'transactions' stamp: 'HAW 5/25/2019 11:38:32'!
transactions
	
	| transactions |
	
	transactions := OrderedCollection new.
	accounts do: [ :anAccount | anAccount addTransactionsTo: transactions ].
	
	^transactions ! !


!Portfolio methodsFor: 'composition' stamp: 'HAW 5/25/2019 12:02:59'!
addedTo: aPortfolio 
	
	parents add: aPortfolio ! !


!Portfolio methodsFor: 'testing' stamp: 'HAW 5/25/2019 12:20:56'!
anyRootParentIsComposedBy: accountToAdd

	^self rootParents anySatisfy: [ :aParent | aParent isComposedBy: accountToAdd]! !

!Portfolio methodsFor: 'testing' stamp: 'HAW 5/25/2019 11:28:29'!
hasRegistered: aTransaction

	^accounts anySatisfy: [ :anAccount | anAccount hasRegistered: aTransaction ]! !

!Portfolio methodsFor: 'testing' stamp: 'HAW 5/29/2019 16:24:54'!
isComposedBy: anAccount

	^ self = anAccount or: [ accounts anySatisfy: [ :composedAccount | (composedAccount isComposedBy: anAccount) or: [ anAccount isComposedBy: composedAccount ]]]! !


!Portfolio methodsFor: 'account management - private' stamp: 'HAW 5/25/2019 12:17:31'!
addRootParentsTo: rootParents

	parents 
		ifEmpty: [ rootParents add: self ] 
		ifNotEmpty: [ parents do: [ :aParent | aParent addRootParentsTo: rootParents ]]! !

!Portfolio methodsFor: 'account management - private' stamp: 'HAW 5/25/2019 12:20:36'!
assertCanAdd: accountToAdd

	(self anyRootParentIsComposedBy: accountToAdd) ifTrue: [ self signalCanNotAddAccount ].
! !

!Portfolio methodsFor: 'account management - private' stamp: 'HAW 5/25/2019 11:48:34'!
signalCanNotAddAccount
	
	self error: self class canNotAddAccountErrorMessage! !


!Portfolio methodsFor: 'report' stamp: 'pm 5/29/2022 19:49:58'!
gatherReportData: aReport

	accounts do: [:anAccount | anAccount gatherReportData: aReport ].
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Portfolio class' category: 'Portfolio-Solucion'!
Portfolio class
	instanceVariableNames: ''!

!Portfolio class methodsFor: 'as yet unclassified' stamp: 'HAW 5/25/2019 11:48:55'!
canNotAddAccountErrorMessage
	
	^'Can not add repeated account to a portfolio'! !

!Portfolio class methodsFor: 'as yet unclassified' stamp: 'HAW 5/25/2019 11:18:21'!
with: anAccount

	^self new 
		add: anAccount;
		yourself! !

!Portfolio class methodsFor: 'as yet unclassified' stamp: 'HAW 5/25/2019 11:23:59'!
with: anAccount with: anotherAccount

	^self new 
		add: anAccount;
		add: anotherAccount;
		yourself! !


!classDefinition: #ReceptiveAccount category: 'Portfolio-Solucion'!
Account subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:35'!
initialize

	super initialize.
	transactions := OrderedCollection new.! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'HAW 5/25/2019 11:38:52'!
addTransactionsTo: aCollectionOfTransactions

	aCollectionOfTransactions addAll: transactions ! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !


!ReceptiveAccount methodsFor: 'balance' stamp: 'HAW 5/25/2019 11:24:46'!
balance

	^transactions 
		inject: 0
		into: [ :currentBalance :transaction | transaction affectBalance: currentBalance ]! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'NR 10/21/2019 18:55:56'!
hasRegistered: aTransaction

	^ transactions includes: aTransaction 
! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'HAW 5/25/2019 11:54:51'!
isComposedBy: anAccount

	^self = anAccount ! !


!ReceptiveAccount methodsFor: 'composition' stamp: 'HAW 5/25/2019 12:03:32'!
addedTo: aPortfolio 
	
	! !


!ReceptiveAccount methodsFor: 'report' stamp: 'pm 5/29/2022 19:32:06'!
gatherReportData: aReport

	transactions do: [ :transaction | transaction includeInReport: aReport ].

	! !


!classDefinition: #AccountTransaction category: 'Portfolio-Solucion'!
Object subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!AccountTransaction methodsFor: 'value' stamp: 'HernanWilkinson 9/12/2011 12:25'!
value 

	self subclassResponsibility ! !


!AccountTransaction methodsFor: 'balance' stamp: 'HAW 5/25/2019 11:25:39'!
affectBalance: aBalance

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: 'Portfolio-Solucion'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'NR 10/21/2019 18:54:27'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #Deposit category: 'Portfolio-Solucion'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!Deposit methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:45'!
initializeFor: aValue

	value := aValue ! !


!Deposit methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !


!Deposit methodsFor: 'balance' stamp: 'HAW 5/25/2019 11:25:02'!
affectBalance: aBalance

	^aBalance + value ! !


!Deposit methodsFor: 'report' stamp: 'mk 5/28/2022 17:51:22'!
includeInReport: aReport

	aReport includeDeposit: self.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: 'Portfolio-Solucion'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #TransferDeposit category: 'Portfolio-Solucion'!
AccountTransaction subclass: #TransferDeposit
	instanceVariableNames: 'transfer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!TransferDeposit methodsFor: 'report' stamp: 'mk 5/28/2022 17:51:36'!
includeInReport: aReport 
	
	aReport includeTransferDeposit: self.! !


!TransferDeposit methodsFor: 'value' stamp: 'pm 5/28/2022 13:56:55'!
value

	^transfer value! !


!TransferDeposit methodsFor: 'balance' stamp: 'peto 5/25/2022 18:26:14'!
affectBalance: aBalance

	^aBalance + transfer value.! !


!TransferDeposit methodsFor: 'transfer' stamp: 'pm 5/28/2022 13:57:41'!
counterpart

	^transfer extraction.! !

!TransferDeposit methodsFor: 'transfer' stamp: 'peto 5/25/2022 19:22:33'!
transference

	^transfer.! !


!TransferDeposit methodsFor: 'initialization' stamp: 'peto 5/25/2022 18:27:38'!
initializeWith: aTransference

	transfer := aTransference.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TransferDeposit class' category: 'Portfolio-Solucion'!
TransferDeposit class
	instanceVariableNames: ''!

!TransferDeposit class methodsFor: 'as yet unclassified' stamp: 'peto 5/25/2022 18:27:03'!
with: aTransference

	^self new initializeWith: aTransference! !


!classDefinition: #TransferExtraction category: 'Portfolio-Solucion'!
AccountTransaction subclass: #TransferExtraction
	instanceVariableNames: 'transfer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!TransferExtraction methodsFor: 'initialization' stamp: 'peto 5/25/2022 18:27:52'!
initializeWith: aTransference

	transfer := aTransference! !


!TransferExtraction methodsFor: 'transfer' stamp: 'pm 5/28/2022 13:57:50'!
counterpart

	^transfer deposit.! !

!TransferExtraction methodsFor: 'transfer' stamp: 'peto 5/25/2022 19:22:48'!
transference

	^transfer.! !


!TransferExtraction methodsFor: 'value' stamp: 'pm 5/28/2022 13:56:40'!
value

	^transfer value! !


!TransferExtraction methodsFor: 'balance' stamp: 'peto 5/25/2022 18:26:04'!
affectBalance: aBalance

	^aBalance - transfer value.! !


!TransferExtraction methodsFor: 'report' stamp: 'mk 5/28/2022 17:51:56'!
includeInReport: aReport 
	
	aReport includeTransferExtraction: self.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TransferExtraction class' category: 'Portfolio-Solucion'!
TransferExtraction class
	instanceVariableNames: ''!

!TransferExtraction class methodsFor: 'as yet unclassified' stamp: 'peto 5/25/2022 18:28:22'!
with: aTransference

	^self new initializeWith: aTransference.! !


!classDefinition: #Withdraw category: 'Portfolio-Solucion'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!Withdraw methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:46'!
initializeFor: aValue

	value := aValue ! !


!Withdraw methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:33'!
value

	^ value! !


!Withdraw methodsFor: 'balance' stamp: 'HAW 5/25/2019 11:25:15'!
affectBalance: aBalance

	^aBalance - value! !


!Withdraw methodsFor: 'report' stamp: 'mk 5/28/2022 17:52:24'!
includeInReport: aReport 
	aReport includeWithdraw: self.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: 'Portfolio-Solucion'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #Report category: 'Portfolio-Solucion'!
Object subclass: #Report
	instanceVariableNames: 'account'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!Report methodsFor: 'transaction' stamp: 'mk 5/28/2022 18:05:29'!
includeDeposit: aDeposit
	self subclassResponsibility. ! !

!Report methodsFor: 'transaction' stamp: 'mk 5/28/2022 18:06:55'!
includeTransferDeposit: aTransferDeposit
	self subclassResponsibility. ! !

!Report methodsFor: 'transaction' stamp: 'mk 5/28/2022 18:07:16'!
includeTransferExtraction: aTransferExtraction
	self subclassResponsibility. ! !

!Report methodsFor: 'transaction' stamp: 'mk 5/28/2022 18:05:59'!
includeWithdraw: aWithdraw
	self subclassResponsibility. ! !


!Report methodsFor: 'present' stamp: 'mk 5/28/2022 18:07:27'!
present
	self subclassResponsibility. ! !


!Report methodsFor: 'initialization' stamp: 'mk 5/28/2022 18:11:11'!
initializeFor: aReceptiveAccount 
	
	account := aReceptiveAccount! !


!classDefinition: #AccountSummary category: 'Portfolio-Solucion'!
Report subclass: #AccountSummary
	instanceVariableNames: 'log'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!AccountSummary methodsFor: 'balance' stamp: 'pm 5/28/2022 14:35:55'!
appendAccountBalance: anAccount
	
	log add: ('Balance = ', (anAccount balance asString)).! !


!AccountSummary methodsFor: 'present' stamp: 'pm 5/29/2022 19:32:55'!
present
		
	log := OrderedCollection new. 
	
	account ifNil:[^log] ifNotNil: [account gatherReportData: self].
	self appendAccountBalance: account.
	
	^log.

	
		! !


!AccountSummary methodsFor: 'transactions' stamp: 'mk 5/28/2022 17:46:19'!
includeDeposit: aDeposit
	
	log add: ('Dep�sito por ', (aDeposit value asString), '.').! !

!AccountSummary methodsFor: 'transactions' stamp: 'mk 5/28/2022 17:47:51'!
includeTransferDeposit: aTransferDeposit

	log add: ('Entrada por transferencia de ', (aTransferDeposit value asString), '.').! !

!AccountSummary methodsFor: 'transactions' stamp: 'mk 5/28/2022 17:47:23'!
includeTransferExtraction: aTransferExtraction
	
	log add: ('Salida por transferencia de ', (aTransferExtraction value asString), '.').! !

!AccountSummary methodsFor: 'transactions' stamp: 'mk 5/28/2022 17:46:51'!
includeWithdraw: aWithdraw 
	
	log add: ('Extracci�n por ', (aWithdraw value asString), '.').! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountSummary class' category: 'Portfolio-Solucion'!
AccountSummary class
	instanceVariableNames: ''!

!AccountSummary class methodsFor: 'as yet unclassified' stamp: 'peto 5/25/2022 19:45:52'!
for: anAccount

	^self new initializeFor: anAccount.! !


!classDefinition: #TrasferNet category: 'Portfolio-Solucion'!
Report subclass: #TrasferNet
	instanceVariableNames: 'neto'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!TrasferNet methodsFor: 'present' stamp: 'mk 5/28/2022 17:40:31'!
present
	
	
	neto:= 0.
	
	account ifNil:[^''] ifNotNil:[account gatherReportData: self].

	
	^'El neto de transferencias es ', neto asString.	
	! !


!TrasferNet methodsFor: 'transactions' stamp: 'mk 5/28/2022 17:46:19'!
includeDeposit: aTranferExtraction
	
	! !

!TrasferNet methodsFor: 'transactions' stamp: 'mk 5/28/2022 17:47:51'!
includeTransferDeposit: aTransferDeposit
	
	neto := neto + aTransferDeposit value.! !

!TrasferNet methodsFor: 'transactions' stamp: 'mk 5/28/2022 17:47:23'!
includeTransferExtraction: aTranferExtraction
	
	neto := neto - aTranferExtraction value.! !

!TrasferNet methodsFor: 'transactions' stamp: 'mk 5/28/2022 17:46:51'!
includeWithdraw: aTranferExtraction
	
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TrasferNet class' category: 'Portfolio-Solucion'!
TrasferNet class
	instanceVariableNames: ''!

!TrasferNet class methodsFor: 'instance creation' stamp: 'mk 5/28/2022 16:49:19'!
for: aReceptiveAccount 
	^self new initializeFor: aReceptiveAccount ! !


!classDefinition: #Transfer category: 'Portfolio-Solucion'!
Object subclass: #Transfer
	instanceVariableNames: 'amount extraction deposit'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Solucion'!

!Transfer methodsFor: 'initialization' stamp: 'pm 5/28/2022 15:32:44'!
initializeFor: anAmount from: senderAccount to: receiverAccount 
	
	amount := anAmount.
	extraction := TransferExtraction with: self.
	deposit := TransferDeposit with: self.
	
	senderAccount register: extraction.
	receiverAccount register: deposit.
	! !


!Transfer methodsFor: 'feets' stamp: 'pm 5/28/2022 13:55:49'!
deposit

	^deposit.! !

!Transfer methodsFor: 'feets' stamp: 'pm 5/28/2022 13:55:36'!
extraction

	^extraction! !


!Transfer methodsFor: 'value' stamp: 'peto 5/25/2022 18:25:42'!
value

	^amount.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Transfer class' category: 'Portfolio-Solucion'!
Transfer class
	instanceVariableNames: ''!

!Transfer class methodsFor: 'initialization' stamp: 'pm 5/28/2022 15:32:54'!
from: senderAccount to: receiverAccount amount: anAmount 

	self assertNotEqualReceiver: receiverAccount andSender: senderAccount.
	self assertAmountMustBeGreaterThanCero: anAmount.
	
	^self new initializeFor: anAmount from: senderAccount to: receiverAccount! !


!Transfer class methodsFor: 'validations' stamp: 'pm 5/28/2022 15:30:53'!
assertAmountMustBeGreaterThanCero: anAmount

	(anAmount <= 0) ifTrue: [^self error: (self mustSendAmountGreaterThanCero)].! !

!Transfer class methodsFor: 'validations' stamp: 'pm 5/28/2022 15:31:12'!
assertNotEqualReceiver: receptiveAccount andSender: senderAccount

	(receptiveAccount == senderAccount) ifTrue: [^self error: self cannotSendMoneyToSelfError].! !


!Transfer class methodsFor: 'error' stamp: 'pm 5/28/2022 15:31:54'!
cannotSendMoneyToSelfError

	^'No es posible enviar dinero a uno mismo.'.! !

!Transfer class methodsFor: 'error' stamp: 'pm 5/28/2022 15:32:20'!
mustSendAmountGreaterThanCero

	^'La cantidad enviada debe ser mayor a 0.'.! !
